# Création de la base `PostgreSQL` Richelieu à partir de jeux de données en CSV

Après la première étape consistant à nettoyer les 4 jeux de données pour préparer l'insertion,
on réalise ici la migration de données vers la base PostgreSQL et l'envoi des fichiers images
sur un serveur distant.

Pour les annuaires: le résultat du travail de Ravinitesh Annapureddy de nettoyage des tableurs se
trouve dans [ce dossier](https://cloud.inha.fr/apps/files/?dir=/Richelieu/Ravinithesh/RICH%20DATA/data/outcome_of_current_project&fileid=666039), en csv.

---

## UTILISATION (GNU-Linux / MacOS)

En supposant:
- que ce projet s'appelle `2_data2sql` et qu'il est à la racine d'un dossier contenant également
  [l'étape 1 de la chaîne de traitement](https://gitlab.inha.fr/snr/rich.data/data-preparation),
  dans un dossier `1_data_preparation` et une fois que cette étape 1 a été lancée (si les noms de
  dossier diffèrent, adapter le script ci-dessous)
- que le dossier `in/` correspond à ce qui est indiqué dans `Structure du projet` plus bas
- qu'il existe des fichiers `src/utils/confidentials/postgresql_credentials{local|remote}.json`
  avec les identifiants de connexion à la base de données souhaitée (voir plus bas)

```bash
# installations system wide
# !! ATTENTION !! les deux commandes suivantes se font au niveau de l'OS,  pas de
# l'environnement virtuel. sauter ces étapes si les programmes sont déjà installés
sudo bash scripts/install_postgresql_14.sh                   # installer postgreSQL 14 si ce n'est pas déjà le cas
sudo apt install libpq-dev                                   # installer la librairie pour communiquer avec PostgreSQL

# installation du projet
git clone https://gitlab.inha.fr/snr/rich.data/data2sql.git  # cloner le dépôt
cd 2_data2sql                                                # se déplacer dans son dossier
python3 -m venv env                                          # créer un environnement virtuel
source env/bin/activate                                      # le sourcer
pip install -r requirements.txt                              # installer les dépendances
cp -r ../1_data_preparation/out/* in/                        # copier les résultats de l'étape 1 en entrée
# ajouter `in/directory/directory.csv`, fichier contenant les données des bottins et annuaires

# lancement
python main.py -d={remote|local}                             # lancer le script d'insertion des données. `-d` pointe vers la BDD à utiliser, "local" ou "remote"
```

---

## STRUCTURE DU PROJET

### Structure globale

Pour plus de clarté, seuls les fichiers importants sont listés, le détail de chaque dossier n'est présenté que si nécessaire.

```
/
|__in/ : les données en entrée
|   |__iconography/       : les données iconographiques, comme produites à la fin de l'étape 1
|   |__cartography/       : les données cartographiques, comme produites à la fin de l'étape 1
|   |__location/          : les données sur les lieux, comme produits à la fin de l'étape 1
|   |__directory/         : un dossier contenant les bottins et annuaires, ajouté à cette étape (pas présent dans l'étape 1)
|       |__directory.csv  : le tableur contenant les bottins et annuaires
|
|__out/ : les fichiers de sortie (dump SQL de la BDD)
|
|__scripts/ : des scripts utiles (installations...)
|
|__src/ : le code de la migration de données
    |__orm/                : modèle de données de la base
    |__processes_data2csv/ : processus lancés par `data2csv.py`
    |__utils/              : fichiers utilitaires
    |   |__confidentials/  : codes d'accès aux bases de données et serveurs
    |__tmp/                : fichiers temporaires (pour sauvegarder les tableurs entre `data2csv` et `csv2sql`)
    |__engine.py           : configuration du moteur de la bdd
    |__data2csv.py         : 1e étape de la migration: préparation et jointure des tables entre elles
    |__csv2sql.py          : 2e étape de la migration: insertion dans la base de données
    |__files2server.py     : ajout des fichiers image sur le serveur distant
```

---

### Le dossier `confidentials`

Le dossier `./src/utils/confidentials/` contient des accès aux BDD et au serveur INHA.

#### PostgreSQL
Des fichiers `postgresql_credentials_local.json` et `postgresql_credentials_remote.json`
donnent les codes d'accès à la BDD locale et la BDD sur serveur; ces fichiers sont ciblés
par l'argument `-d --database` du CLI:
- `-d=local` pour cibler `postgresql_credentials_local.json`
- `-d=remote` pour cibler `postgresql_credentials_remote.json`
- en théorie, il est possible de se connecter à d'autres BDD en ajoutant des fichiers
  `confientials/postgresql_credentials_*.json`, en ajoutant des arguments à `-d`
  et en modifiant les fonctions `src/utils/io/{write|read}_credfile.py`
- les jsons doivent avoir la structure suivante:
```javascript
{
  "username": "<utilisateur de la bdd>",
  "password": "<code d'accès>"
  "uri": "<uri du serveur postgresql>",
  "db": "<nom de la base postgresql>"
}
```

#### SSH
Un fichier `./src/utils/confidentials/ssh_credentials.json`, contenant les codes
d'accès au serveur SSH, ayant la structure suivante (une connexion filaire ou un
VPN sont nécessaires pour accéder au serveur INHA):
```javascript
{
  "username": "<nom d'utilisateur>",
  "password": "<code>",
  "server": "<url du serveur>",
  "basepath": "<chemin absolu vers le dossier de travail racine richelieu>",
  "imagepath": "<chemin absolu vers le dossier contenant les images richelieu>"
}
```

---

## OBJECTIFS

Quatres tableurs (iconographie, cartographie, bottins et annuaires, lieux) sont traités avec `pandas` pour réaliser
une base de données PostgreSQL avec `sqlalchemy`.

**Iconographie**
| index | author_1 | author_2 | corpus | date_corr | date_norm | date_source | description | id_location | inscription | institution | note_richelieh | inventory_number | named_entity | producted_richelieu | publisher | represents_richelieu | technique | theme | title | title_2 | url_manifest | url_source | filenames |
| ----- | -------- | -------- | ------ | --------- | --------- | ----------- | ----------- | ----------- | ----------- | ----------- | ------------- | ---------------- | ------------ | ------------------- | --------- | -------------------- | --------- | ----- | ----- | ------- | ------------ | ---------- | --------- |
| index ou indentifiant de la ressource | auteur.ice principal.e | autre auteur.ice | corpus auquel appartient l'oeuvre | date corrigée par l'équipe Richelieu | date normalisée (format `[YYYY-YYYY]` | date dans la source | description de la ressource | identifiant des lieux représentés | inscription sur l'oeuvre | institution conservant l'oeuvre | note interne au projet Richelieu | numéro d'inventaire | entité nommée | producteur.ice de la notice au sein du projet Richelieu | éditeur | sujet représenté, selon notre équipe | technique | thème | titre | titre secondaire | url vers le manifeste IIIF | url de la source en ligne | noms des fichiers image associés |

**Cartographie**
| institution | filename | description | addresses | id_location | corpus | url_source | year | note_richelieu | url_image |
| ----------- | -------- | ----------- | --------- | ----------- | ------ | ---------- | ---- | -------------- | --------- |
| institution conservant le document | nom du fichier image | description de la ressource | addresses administratives représentées | identifiant du lieu représenté | corpus auquel appartient la carte / références externes | url de la ressource | année de production | note interne | addresse de téléchargement de l'image |

**Bottins et annuaires**
| ark_gallica | row | name_ | occupation | street | number | date | gallica_page | tags |
| ----------- | --- | ----- | ---------- | ------ | ------ | ---- | ------------ | ---- |
| identifiant de la ressource sur Gallica | ligne de la page du bottin | nom de la personne | occupation de la personne | rue d'activité | numéro de rue | date du bottin | numéro de page sur Gallica | tags/ informations extraites |

**Lieux / localisations**
| index | current | atlas1860 | feuille1837 | vasserot1810 |
| ----- | ------- | --------- | ----------- | ------------ |
| index | addresse actuelle | atlas de 1860 | parcelles à la feuille de 1837 | atlas Vasserot de 1810 |

Ces quatre tableurs sont croisés pour former une base de données correspondant **au modèle suivant**:

![capture d'écran de la base de données](img/richdata_modele_relationnel_20230203.png)

---

## ORM ET DESIGN DE LA BASE DE DONNÉES

### Base de données

- la base de données utilise un **système d'identifiants doubles**:
  - **`id`**: la `PRIMARY KEY`, type `int` est une clé primaire interne qui
    n'est jamais exposée au public
  - **`uuid`**: un identifiant pérenne exposé au public au format `qr1{UUID}`.
  - ce système de double identifiants permet:
    - d'avoir un identifiant pérenne pour chaque ressource
    - de faire les requêtes et jointures sur un `int`: c'est beaucoup plus
      performant et ça permet des requêtes de type `SORT BY id`.
    - la clé primaire n'est jamais exposée au public, ce qui est mieux pour
      des questions de sécurité
  - c'est généralement la solution vers laquelle pointent les réponses en ligne:
    [ce thread](https://stackoverflow.com/questions/52414414/best-practices-on-primary-key-auto-increment-and-uuid-in-sql-databases)    et [cet article](https://tomharrisonjr.com/uuid-or-guid-as-primary-keys-be-careful-7b2aa3dcb439)
    (il y a aussi des problèmes avec cette solution, voir le commentaire de l'article).
  - sur les tables `iconography` et `place`, il y a un troisième identifiant: `**id_richelieu**`, 
    qui permet de conserver l'identifiant créé manuellement pour chaque entrée. 
- **`date`**: toutes les colonnes de dates sont représentées par un `INT4RANGE`, soit une
  tranche d'entiers.
  - dans PostgreSQL, la limite haute d'une tranche d'entiers
    est convertie à l'entier supérieur: `[1801,1871]` devient `[1801,1872)` (c'est-à-dire
    *1801 inclus jusqu'à 1871 exclu*, au lieu de *1801 inclus jusqu'à 1871 inclus*)
  - c'est la même chose avec les scalaires: `1801` devient `[1801,1802)`. il faudra donc reconvertir
    avant de présenter les dates à l'utilisateur.ice.
  - voir ces articles: sur [l'inclusivité des tranches](https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-INCLUSIVITY),
    sur [l'IO](https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-IO)
    et surtout [**l'explication des tranches d'entier discrètes**](https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-DISCRETE)

### ORM Sqlalchemy

Dans l'ORM, les **tables de relations utilisent le style `Association Object`**
(voir [la documentation SQLAlchemy](https://docs.sqlalchemy.org/en/20/orm/basic_relationships.html))

---

## CHAÎNE DE TRAITEMENT

- `data2csv.py`: les tableurs complets présents en source sont éclatés en plusieurs petits tableurs `csv`, chaque
  tableur correspondant à une table SQL.
- `csv2sql.py`: les tableurs produits à l'étape précédente sont insérés dans la base de données PostgreSQL.
- `engine.py/rundump()`: faire un dump de la base de données

---

## À PROPOS `PostgreSQL` ET `SQLAlchemy`

pour utiliser `SQLAlchemy` avec `PostgreSQL`, utiliser **`psycopg`**.
- après des erreurs avec l'installation de postgresql en local, j'ai du bidouiller une solution. voir
  `postgresql_fix.md` pour plus de détails sur l'erreur et la solution.
- en bref, `SQLAlchemy` pour `postgresql` repose sur la DBAPI `psycopg` et elle même repose sur la
  bibliothèque `libsql`, qu'il faut installer avec:
  ```bash
  sudo apt install libpq-dev
  ```

---

## LICENCE ET CRÉDITS

Code source sous GNU GPL 3.0, données sous licence CC-BY-SA 4.0.
