from sqlalchemy import text
import argparse

from src.utils.io import deltmp, maketmp, makeout, write_credfile
from src.data2csv import pipeline_data2csv
from src.csv2sql import pipeline_csv2sql
from src.orm import Base


# *******************************************
# CLI to transform the source datasets
# into a postgreSQL database and push
# extra files on the INHA servers
# *******************************************


if __name__ == "__main__":
    # cli logic
    parser = argparse.ArgumentParser(prog="data2sql"
                                     , description="create the RICH.DATA SQL database from CSV inputs")
    parser.add_argument("-d", "--database"
                        , required=True
                        , choices=["local", "remote"]
                        , help="database to use (local or remote)")
    parser.add_argument("-k", "--keeptmp"
                        , action="store_true"
                        , help="if this flag is used, the temp files in `./src/tmp` "
                               + "will not be deleted at the end of this script.")
    args = parser.parse_args()

    # empty and delete + recreate the TMP directories
    deltmp()
    maketmp()
    makeout()

    # create the engine and the database structure
    write_credfile(args.database)
    from src.engine import engine  # avoid import errors
    if len(Base.metadata.tables.keys()) != 0:
        Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # split the source datasets into CSV representations of the SQL tables
    pipeline_data2csv()

    # write the CSV data to our SQL tables
    pipeline_csv2sql()

    # make a database dump
    from src.engine import rundump  # avoid import errors
    rundump()

    # n° of rows inserted
    stmt = """
        WITH data AS (
            SELECT schemaname,relname,n_live_tup
            FROM pg_stat_user_tables
        )
        SELECT SUM(data.n_live_tup)
        FROM data;
    """
    with engine.begin() as conn:
        r = conn.execute(text(stmt))
    out = (f"* {r.scalar()} rows inserted into database. database creation done !"
          + "\n* database dump written to `out/richelieu_dump.sql` and `out/richelieu_dump_schema.sql`")
    stars = "***************************************************************************************"
    print("")
    print(stars)
    print(out)
    print(stars)
    print("")

    # delete temp files
    if not args.keeptmp:
        deltmp()

