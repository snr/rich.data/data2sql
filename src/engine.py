from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from datetime import datetime
import subprocess
import json
import os

from .utils.constants import CONFIDENTIALS, OUT
from .utils.io import read_credfile


# *******************************************************
# postgreSQL specific functions:
# * create an engine with `build_engine`
# * create a database dump with `dump`
# *******************************************************


def rundump() -> None:
    """
    create a script to create a database dump
    and save it to `OUT/richelieu_dump.sql`
    the credentials file to use is indicated by the cli's
    `-d --database` argument and stored in `TMP/credfile.txt`
    """
    timestamp = datetime.now().strftime(r"%Y%m%d")
    print(timestamp)
    fp_dump_full = os.path.join(OUT, f"{timestamp}_richelieu_dump.sql")
    fp_dump_schema = os.path.join(OUT, f"{timestamp}_richelieu_dump_schema.sql")
    fn = read_credfile()
    with open(os.path.join(CONFIDENTIALS, fn)) as fh:
        cred = json.load(fh)
    # dump of the full database (with data)
    cmd_full = (f"pg_dump -C -h '{cred['uri']}' -d '{cred['db']}' -U '{cred['username']}' --no-owner --clean"
               + f"> { fp_dump_full }")
    # dump of the database's schema only
    cmd_schema = (f"pg_dump -s -h '{cred['uri']}' -d '{cred['db']}' -U '{cred['username']}' --no-owner --clean"
                 + f"> { fp_dump_schema }")

    print(f"\n* creating database dumps."
         + "\n**************************")
    print(f"* running: `{cmd_full}`\n* password is `{ cred['password'] }`\n")
    subprocess.run(cmd_full, check=True, shell=True)  # run the dump in a subprocess
    print(f"* running: `{cmd_schema}`\n* password is `{ cred['password'] }`\n")
    subprocess.run(cmd_schema, check=True, shell=True)  # run the dump in a subprocess

    return


def build_engine() -> Engine:
    """
    create an SQLAlchemy PostgreSQL engine
    the credentials file to use is indicated by the cli's
    `-d --database` argument and stored in `TMP/credfile.txt`
    """
    # read the credentials
    fn = read_credfile()
    with open(os.path.join(CONFIDENTIALS, fn)) as fh:
        cred = json.load(fh)
        print(cred)

    # create the engine
    return create_engine(
        f"postgresql://{ cred['username'] }:{ cred['password'] }@{ cred['uri'] }/{ cred['db'] }"
        , echo=True
    )


engine = build_engine()

