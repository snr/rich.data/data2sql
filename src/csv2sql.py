from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from sqlalchemy import types as sa_types
from psycopg2.extras import NumericRange
from sqlalchemy.orm import Session
from typing import List, Dict
import pandas as pd
import numpy as np
import json
import ast
import re
import os

from .utils.io import read_df


# ********************************************
# read CSV files stored in `temp` and insert
# the data they contain in the SQL database
# ********************************************


class Csv2Sql():
    """
    class holding all dataframes and variables for our csv -> sql transforms.
    """
    def __init__(self):
        from .engine import engine  # necessary to do it here to avoid import errors
        self.engine = engine

        self.df_filename    = read_df("filename", useindexcol=0, customheader=True)
        self.df_address     = read_df("address", useindexcol=0, customheader=True)
        self.df_place       = read_df("place", useindexcol=0, customheader=True)
        self.df_directory   = read_df("directory", useindexcol=0, customheader=True)
        self.df_iconography = read_df("iconography", useindexcol=0, customheader=True)
        self.df_cartography = read_df("cartography", useindexcol=0, customheader=True)

        self.df_actor        = read_df("actor", useindexcol=0, customheader=True)
        self.df_theme        = read_df("theme", useindexcol=0, customheader=True)
        self.df_title        = read_df("title", useindexcol=0, customheader=True)
        self.df_licence      = read_df("licence", useindexcol=0, customheader=True)
        self.df_annotation   = read_df("annotation", useindexcol=0, customheader=True)
        self.df_institution  = read_df("institution", useindexcol=0, customheader=True)
        self.df_admin_person = read_df("admin_person", useindexcol=0, customheader=True)
        self.df_named_entity = read_df("named_entity", useindexcol=0, customheader=True)
        self.df_place_group  = read_df("place_group", useindexcol=0, customheader=True)

        self.df_r_institution              = read_df("r_institution", useindexcol=0, customheader=True)
        self.df_r_admin_person             = read_df("r_admin_person", useindexcol=0, customheader=True)
        self.df_r_address_place            = read_df("r_address_place", useindexcol=0, customheader=True)
        self.df_r_iconography_actor        = read_df("r_iconography_actor", useindexcol=0, customheader=True)
        self.df_r_iconography_theme        = read_df("r_iconography_theme", useindexcol=0, customheader=True)
        self.df_r_iconography_place        = read_df("r_iconography_place", useindexcol=0, customheader=True)
        self.df_r_cartography_place        = read_df("r_cartography_place", useindexcol=0, customheader=True)
        self.df_r_iconography_named_entity = read_df("r_iconography_named_entity", useindexcol=0, customheader=True)
        return


    def pipeline(self):
        """
        main processing pipeline
        """
        print("inserting data in the postgreSQL database")
        print("*****************************************")
        (self.float2int()
             .empty2nan()
             .dropnan()
             .dropcascade()
             .sql_retypes()
             .df2sql()
        )
        return self


    def empty2nan(self):
        """
        replace empty lists / dicts in list/dict columns by np.nan on
        all columns of all dataframes.
        all our columns are non-mixed type, which means that, in list or
        dict columns, absence of value for a row is signified by `[]` or `{}`
        (empty list or dict).
        for SQL, it is better to have NULL / NaN rows rather than empty
        (`[]`, `{}`) rows, hence the retyping.
        """
        def retype(col:SType) -> SType:
            """
            retype non nan values in a column to list or dict, depending on
            their original dtype: they have been retyped to string by `v.mask`
            operations
            """
            if ( col.loc[col.notna()].astype(str).str.startswith(("[", "{")).all()
                 and not col.isna().all() ):
                col.loc[col.notna()] = col.loc[col.notna()].apply(ast.literal_eval)
            return col

        for k,v in self.__dict__.items():
            if re.search("^df", k):
                df = v.copy()
                df = df.mask( df.map(str).isin(["[]", "{}"]) ).apply(retype, axis=0)
                setattr(self, k, df)
        return self


    def float2int(self):
        """
        columns that contain foreign keys and np.nan values are converted
        to `float64` because the `int` dtype cannot hold NaN values.
        => convert those columns to pandas's `Int64`, which can hold NaN
        values with ints
        """
        for k,v in self.__dict__.items():
            if re.search("^df", k):
                df = v.copy()
                toretype = df.select_dtypes(include="float64").columns
                df[toretype] = df[toretype].astype("Int64")
                setattr(self, k, df)
        return self


    def dropnan(self):
        """
        drop rows from all dataframe that contain only nan values
        on "important" colums.
        since we use `dropna()`, columns that don't contain scalars
        must be removed from the `subset` argument.
        relationship and qualifier columns are mostly not processed
        because deleting rows would cause too many foreign keys to
        point to nothing.
        """
        #############################################
        # THIS FUNCTION COULD EVOLVE TO MATCH
        # THE CONSTRAINTS OF THE SQL MODEL
        #############################################

        self.df_actor = self.df_actor.dropna(how="all", axis=0, subset=["entry_name"])
        self.df_address = self.df_address.dropna(how="all", axis=0, subset=["address"])
        self.df_iconography = self.df_iconography.dropna( how="all", axis=0
                                                        , subset=["corpus", "date_source", "description"
                                                                 , "inscription", "inventory_number"
                                                                 , "iiif_url", "source_url"])
        self.df_address = self.df_address.dropna( how="all", axis=0
                                                , subset=["city", "country", "date", "address", "source"])
        self.df_directory = self.df_directory.dropna( how="all", axis=0
                                                    , subset=[ "date", "gallica_ark", "gallica_page"
                                                             , "gallica_row", "entry_name", "occupation"])
        return self


    def dropcascade(self):
        """
        propagate deletion of rows dropped with `self.dropnan()`:
        `dropnan()` deletes rows in dataframes, causing foreign key errors:
        foreign keys will target dataframe rows that have been dropped.
        """
        def dropper(df_from_name:str, mapper:Dict) -> None:
            """
            delete rows in `df_from` with foreign keys pointing to rows
            in target dataframes that have been deleted by `dropnan()`

            :param df_from_name: the name of the df containing the foreign keys
            :param mapper: a dict mapping foreign key columns to names of target dfs:
                           { <foreign key column>: <name of target dataframe> }
            """
            # get the dataframes
            df_from = getattr(self, df_from_name)
            prelen  = df_from.shape[0]
            fk_cols = list(mapper.keys())                                # foreign key columns
            df_tgts = { k: getattr(self, v) for k,v in mapper.items() }  # { <foreign key column>: <target df> }

            # check that foreign keys are valid using `valid` and `allna`, two filter columns
            df_from["valid"] = [ [] for _ in range(df_from.shape[0]) ]                # array of bool: does each col in `fk_cols` point to a valid primary key in the target dataframe ?
            df_from["allna"] = df_from[fk_cols].isna().all(axis=1)                    # all rows are empty
            for col in fk_cols:
                df_from.apply(lambda x: x.valid.append( x[col] in df_tgts[col].index  # foreign key points to a valid target
                                                        or pd.isna(x[col]) )          # no foreign key
                              , axis=1)

            # keep only valid rows in the df
            df_from.valid = df_from.valid.apply(all)
            df_from = (df_from.loc[ df_from.valid & ~df_from.allna ]
                              .drop( columns=["valid", "allna"] ))

            # update `self`
            print(f"* { prelen - df_from.shape[0] } out of { prelen } rows dropped in "
                  + f"`{ df_from_name }`: foreign key(s) in `{ df_from_name }.{ fk_cols }`"
                  + f"pointed to non existing row(s) in one of `{ list(mapper.values()) }`.")
            setattr(self, df_from_name, df_from)
            return

        dropper("df_r_iconography_theme"        , { "id_iconography": "df_iconography" })
        dropper("df_r_iconography_named_entity" , { "id_iconography": "df_iconography" })
        dropper("df_filename"                   , { "id_iconography": "df_iconography",
                                                    "id_cartography": "df_cartography" })
        dropper("df_r_institution"              , { "id_iconography": "df_iconography",
                                                    "id_cartography": "df_cartography",
                                                    "id_directory"  : "df_directory" })
        dropper("df_r_admin_person"             , { "id_iconography": "df_iconography",
                                                    "id_cartography": "df_cartography",
                                                    "id_directory"  : "df_directory" })
        dropper("df_r_iconography_actor"        , { "id_iconography": "df_iconography",
                                                    "id_actor"      : "df_actor" })
        dropper("df_r_address_place"            , { "id_address"    : "df_address" })
        return self


    def sql_retypes(self):
        """
        retype from python types to postgreSQL types.

        ******** important details ********
        `[]` bounds indicate that the range is includes the lower and
        upper bounds ([1738,1900] -> range from 1738 to 1900 included).
        this avoids empty ranges when `<upper bound> == <lower bound>`.
        in PostgreSQL, INT4RANGE is a discrete range type (see link below)
        => the upper bound is rounded to n+1: `[1738,1900]` becomes `[1738,1901)`
        `()` indicate a non-inclusive range, and `[)` a range that is
        non-inclusive at the end.
        see:
        * https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-INCLUSIVITY
        * https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-IO
        * https://www.postgresql.org/docs/current/rangetypes.html#RANGETYPES-DISCRETE
        """
        def list2numrange(cell:List[int]) -> NumericRange|np.float64:
            if type(cell) != list and pd.isna(cell):
                return cell
            return NumericRange( lower=int(min(cell))
                               , upper=int(max(cell))
                               , bounds="[]" )  # see docstring
        self.df_iconography.date = self.df_iconography.date.apply(list2numrange)
        self.df_cartography.date = self.df_cartography.date.apply(list2numrange)
        self.df_directory.date = self.df_directory.date.apply(list2numrange)
        self.df_place.date = self.df_place.date.apply(list2numrange)
        self.df_address.date = self.df_address.date.apply(list2numrange)
        return self


    def df2sql(self):
        """
        insert all our data to the database table. to avoid
        integrity issues, the order of table population is
        important
        """
        # `engine.begin()` allows SQLAlchemy to rollback if
        # an error occurs: https://stackoverflow.com/a/70945288/17915803
        with self.engine.begin() as conn:
            p = { "con": conn, "if_exists": "append", "index_label": "id" }  # pandas `to_sql` parameters
            # ok
            self.df_licence.to_sql("licence", **p)
            # ok
            self.df_admin_person.to_sql("admin_person", **p)
            # ok
            self.df_institution.to_sql("institution", **p)
            # ok with soft constraints
            self.df_iconography.to_sql("iconography", **p)
            # ok with soft constraints
            self.df_cartography.to_sql("cartography", **p, dtype={ "vector": sa_types.JSON })  # avoid `psycopg2` ProgrammingError on dict columns
            # ok
            self.df_address.to_sql("address", **p)
            # ok
            self.df_directory.to_sql("directory", **p)
            # ok
            self.df_theme.to_sql("theme", **p)
            # ok
            self.df_named_entity.to_sql("named_entity", **p)
            # ok
            self.df_actor.to_sql("actor", **p)
            # ok (but the table is empty)
            self.df_place_group.to_sql("place_group", **p)
            # ok
            self.df_place.to_sql("place", **p, dtype={ "vector": sa_types.JSON, "centroid": sa_types.JSON })
            # ok with `dropcascade()`
            self.df_filename.to_sql("filename", **p)
            # ok
            self.df_title.to_sql("title", **p)
            # ok but the table is empty
            self.df_annotation.to_sql("annotation", **p)
            # ok
            self.df_r_cartography_place.to_sql("r_cartography_place", **p)
            # ok but empty df
            self.df_r_iconography_place.to_sql("r_iconography_place", **p)
            # ok after `dropcascade()`
            self.df_r_iconography_theme.to_sql("r_iconography_theme", **p)
            # ok after `dropcascade()`
            self.df_r_iconography_named_entity.to_sql("r_iconography_named_entity", **p)
            # ok
            self.df_r_iconography_actor.to_sql("r_iconography_actor", **p)
            # ok after `dropcascade()`
            self.df_r_address_place.to_sql("r_address_place", **p)
            # ok with `dropcascade()`
            self.df_r_institution.to_sql("r_institution", **p)
            # ok with `dropcascade()`
            self.df_r_admin_person.to_sql("r_admin_person", **p)

        print("")
        print("* database creation done !\n")
        return self


def pipeline_csv2sql():
    """
    insert all data from all 23 dataframes to our Postgre database.
    """
    Csv2Sql().pipeline()

    return


    # def name_2name(self):
    #     """
    #     all columns named `name` in the postgres database are named
    #     `entry_name` in the dataframes, because `name` is a keyword of a
    #     dataframe and this means we lose some functionnality. now
    #     that we don't do complex dataframe operations, rename `entry_name`
    #     columns to `name`.
    #     """
    #     for k,v in self.__dict__.items():
    #         if re.search("^df", k):
    #             df = v.copy()
    #             df = df.rename(columns={ "entry_name": "name" })
    #             setattr(self, k, df)
    #     return self


