import pandas as pd
import numpy as np
import re

from ..utils.constants import INSTITUTIONS, ADMIN_PERSON, DATASETS, LICENSE
from ..utils.strings import build_uuid
from ..utils.io import write_ro, write_df


class OtherProcesses():
    """
    all processes to create auxiliary SQL tables that don't have a
    data source in `in/`.
    """
    def __init__(self):
        pass

    def pipeline(self):
        """
        main pipeline for the secondary tables / processes
        that don't derive from a data source in `in/`
        """
        (self.headers2csv()
             .admin_person2csv()
             .institution2csv()
             .licence2csv()
         )
        return self

    def headers2csv(self):
        """
        create all the necessary temp dataframes
        (with headers only) and write them to csv
        """
        for k in DATASETS.keys():
            # do not write the input dataframe to `TMP`
            if not re.search("^in_", k):
                write_ro( k
                        , pd.Series( DATASETS[k][0][1:]         # other column names
                                   , name=DATASETS[k][0][0] ))  # index column name: "index", first item of `DATASETS[k][0]`

        return self

    def institution2csv(self):
        """
        write the `institution` csv to `TMP`
        """
        df = pd.DataFrame.from_dict({ build_uuid(): { "entry_name": _[1], "description": np.nan }
                                      for _ in INSTITUTIONS }
                                    , orient="index")
        write_df("institution", df, customheader=False, append=False)
        return self

    def admin_person2csv(self):
        """
        write the `admin_person` csv to `TMP`
        """
        df = pd.DataFrame.from_dict({ build_uuid(): { "first_name"    : _[0],
                                                      "last_name"     : _[1],
                                                      "id_persistent" : _[2] }
                                      for _ in ADMIN_PERSON }
                                    , orient="index")
        write_df("admin_person", df, customheader=False, append=False)
        return self

    def licence2csv(self):
        """
        write the `licence` csv to `Tmp`
        """
        df = pd.DataFrame.from_dict({ build_uuid(): { "entry_name": _[0], "description" : _[1] }
                                      for _ in LICENSE }
                                    , orient="index")
        write_df("licence", df, customheader=False, append=False)
        return self


