from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
import pandas as pd
import numpy as np
import typing as t
import ast
import re

from ..utils.functions import build_uuid
from ..utils.io import read_df, write_df
from ..utils.constants import DATASETS
from .shared import SharedProcesses

from .. import orm

class RelationshipProcesses(SharedProcesses):
    """
    from the other tables processed before,
    create the relationship tables and other
    auxiliary tables and save them to csv files
    """
    def __init__(self):
        self.df_filename    = read_df("filename", useindexcol=0, customheader=False)
        self.df_address     = read_df("address", useindexcol=0, customheader=False)
        self.df_place       = read_df("place", useindexcol=0, customheader=False)
        self.df_directory   = read_df("directory", useindexcol=0, customheader=False)
        self.df_iconography = read_df("iconography", useindexcol=0, customheader=False)
        self.df_cartography = read_df("cartography", useindexcol=0, customheader=False)

        self.df_licence      = read_df("licence", useindexcol=0, customheader=False)
        self.df_institution  = read_df("institution", useindexcol=0, customheader=False)
        self.df_admin_person = read_df("admin_person", useindexcol=0, customheader=False)

        self.df_actor        = read_df("actor", useindexcol=0, customheader=False)
        self.df_theme        = read_df("theme", useindexcol=0, customheader=False)
        self.df_title        = read_df("title", useindexcol=0, customheader=False)
        self.df_annotation   = read_df("annotation", useindexcol=0, customheader=False)
        self.df_named_entity = read_df("named_entity", useindexcol=0, customheader=False)
        self.df_place_group  = read_df("place_group", useindexcol=0, customheader=False)

        self.df_r_institution              = read_df("r_institution", useindexcol=0, customheader=False)
        self.df_r_admin_person             = read_df("r_admin_person", useindexcol=0, customheader=False)
        self.df_r_address_place            = read_df("r_address_place", useindexcol=0, customheader=False)
        self.df_r_iconography_actor        = read_df("r_iconography_actor", useindexcol=0, customheader=False)
        self.df_r_iconography_theme        = read_df("r_iconography_theme", useindexcol=0, customheader=False)
        self.df_r_iconography_place        = read_df("r_iconography_place", useindexcol=0, customheader=False)
        self.df_r_cartography_place        = read_df("r_cartography_place", useindexcol=0, customheader=False)
        self.df_r_iconography_named_entity = read_df("r_iconography_named_entity", useindexcol=0, customheader=False)

        # dict of `{ <dataframe name>: <DATASETS key> }`, used for debugs
        self.mapper2datasets = {
            "df_address"    : "address",
            "df_filename"   : "filename",
            "df_directory"  : "directory",
            "df_iconography": "iconography",
            "df_cartography": "cartography",
            "df_place"      : "place",

            "df_licence"     : "licence",
            "df_institution" : "institution",
            "df_admin_person": "admin_person",

            "df_actor"        : "actor",
            "df_theme"        : "theme",
            "df_title"        : "title",
            "df_annotation"   : "annotation",
            "df_named_entity" : "named_entity",
            "df_place_group"  : "place_group",

            "df_r_institution"             : "r_institution",
            "df_r_admin_person"            : "r_admin_person",
            "df_r_address_place"           : "r_address_place",
            "df_r_iconography_actor"       : "r_iconography_actor",
            "df_r_iconography_theme"       : "r_iconography_theme",
            "df_r_iconography_place"       : "r_iconography_place",
            "df_r_cartography_place"       : "r_cartography_place",
            "df_r_iconography_named_entity": "r_iconography_named_entity",
        }

        self.mapper2orm = {
            "df_address"    : orm.Address,
            "df_filename"   : orm.Filename,
            "df_directory"  : orm.Directory,
            "df_iconography": orm.Iconography,
            "df_cartography": orm.Cartography,
            "df_place"      : orm.Place,

            "df_licence"     : orm.Licence,
            "df_institution" : orm.Institution,
            "df_admin_person": orm.AdminPerson,

            "df_actor"       : orm.Actor,
            "df_theme"       : orm.Theme,
            "df_title"       : orm.Title,
            "df_annotation"  : orm.Annotation,
            "df_named_entity": orm.NamedEntity,
            "df_place_group" : orm.PlaceGroup,

            "df_r_institution"             : orm.R_Institution,
            "df_r_admin_person"            : orm.R_AdminPerson,
            "df_r_address_place"           : orm.R_AddressPlace,
            "df_r_iconography_actor"       : orm.R_IconographyActor,
            "df_r_iconography_theme"       : orm.R_IconographyTheme,
            "df_r_iconography_place"       : orm.R_IconographyPlace,
            "df_r_cartography_place"       : orm.R_CartographyPlace,
            "df_r_iconography_named_entity": orm.R_IconographyNamedEntity
        }
        return

    def pipeline(self):
        """
        processing pipeline. basically we use `build_relationship`
        on all necessary columns to create all the relationship tables
        """
        # *********************** CLEANING  *********************** #
        self.retype2list().uuid2int()

        # check that all ids in `df_iconography.id_place` are valid
        matcher = self.df_iconography.id_place.explode().str.strip().str.match("^qr1")
        assert matcher.all(), \
               (f"relationships.pipeline(): {matcher.sum()} "
               + "values in in `df_iconography.id_place` are not valid UUIDs.")

        # no licence defined for image files so far => add the UUID to CC-BY
        self.df_filename["id_licence"] = (self.df_licence.loc[ self.df_licence.entry_name == "CC BY 4.0" ]
                                                     .squeeze()
                                                     .name)

        # *********************** UPDATE FOREIGN KEYS *********************** #
        # foreign keys point to uuids but the index is now a `pd.RangeIndex()`,
        # not an uuid => update the foreign keys so that they point to the new index
        print("* replacing uuid indexes by integers and updating foreign keys")
        (self.foreignkey2int("df_directory", "df_institution", "id_institution")
             .foreignkey2int("df_directory", "df_admin_person", "id_admin_person")
             .foreignkey2int("df_directory", "df_licence", "id_licence")
             .foreignkey2int("df_directory", "df_address", "id_address")

             .foreignkey2int("df_iconography", "df_theme", "id_theme")
             .foreignkey2int("df_iconography", "df_named_entity", "id_named_entity")
             .foreignkey2int("df_iconography", "df_actor", "id_author")
             .foreignkey2int("df_iconography", "df_actor", "id_publisher")
             .foreignkey2int("df_iconography", "df_institution", "id_institution")
             .foreignkey2int("df_iconography", "df_admin_person", "id_admin_person")
             .foreignkey2int("df_iconography", "df_place", "id_place")
             .foreignkey2int("df_iconography", "df_licence", "id_licence")

             .foreignkey2int("df_cartography", "df_place", "id_place")
             .foreignkey2int("df_cartography", "df_institution", "id_institution")
             .foreignkey2int("df_cartography", "df_admin_person", "id_admin_person")
             .foreignkey2int("df_cartography", "df_licence", "id_licence")

             .foreignkey2int("df_place", "df_address", "id_address")
             .foreignkey2int("df_place", "df_place_group", "id_place_group")

             .foreignkey2int("df_filename", "df_iconography", "id_iconography")
             .foreignkey2int("df_filename", "df_cartography", "id_cartography")
             .foreignkey2int("df_filename", "df_licence", "id_licence")

             .foreignkey2int("df_title", "df_iconography", "id_iconography")
             .foreignkey2int("df_annotation", "df_iconography", "id_iconography")
        )

        # *********************** JOINING *********************** #
        # directory joins
        print("* building relationship tables\n")
        (self.build_relationship( "df_directory"
                                , "df_r_institution"
                                , df_from_col="id_institution"
                                , df_rel_from="id_directory"
                                , df_rel_to="id_institution"
                                , extra_keyval={ "id_iconography": np.nan, "id_cartography": np.nan })
             .build_relationship( "df_directory"
                                , "df_r_admin_person"
                                , df_from_col="id_admin_person"
                                , df_rel_from="id_directory"
                                , df_rel_to="id_admin_person"
                                , extra_keyval={ "id_iconography": np.nan, "id_cartography": np.nan })

             # iconography joins
             .build_relationship( "df_iconography"
                                , "df_r_iconography_theme"
                                , df_from_col="id_theme"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_theme")
             .build_relationship( "df_iconography"
                                , "df_r_iconography_named_entity"
                                , df_from_col="id_named_entity"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_named_entity")
             .build_relationship( "df_iconography"
                                , "df_r_iconography_actor"
                                , df_from_col="id_author"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_actor"
                                , extra_keyval={ "role": "author" }
                                , do_ismain=True)
             .build_relationship( "df_iconography"
                                , "df_r_iconography_actor"
                                , df_from_col="id_publisher"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_actor"
                                , extra_keyval={ "ismain": True, "role": "publisher" })
             .build_relationship( "df_iconography"
                                , "df_r_institution"
                                , df_from_col="id_institution"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_institution"
                                , extra_keyval={ "id_cartography": np.nan, "id_directory": np.nan })
             .build_relationship( "df_iconography"
                                , "df_r_admin_person"
                                , df_from_col="id_admin_person"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_admin_person"
                                , extra_keyval={ "id_cartography": np.nan, "id_directory": np.nan })
             .build_relationship( "df_iconography"  # no place foreign keys so far but whatever
                                , "df_r_iconography_place"
                                , df_from_col="id_place"
                                , df_rel_from="id_iconography"
                                , df_rel_to="id_place")

             # cartography joins
             .build_relationship( "df_cartography"
                                , "df_r_cartography_place"
                                , df_from_col="id_place"
                                , df_rel_from="id_cartography"
                                , df_rel_to="id_place")
             .build_relationship( "df_cartography"
                                , "df_r_institution"
                                , df_from_col="id_institution"
                                , df_rel_from="id_cartography"
                                , df_rel_to="id_institution"
                                , extra_keyval={ "id_iconography": np.nan,
                                                 "id_directory"  : np.nan })
             .build_relationship( "df_cartography"
                                , "df_r_admin_person"
                                , df_from_col="id_admin_person"
                                , df_rel_from="id_cartography"
                                , df_rel_to="id_admin_person"
                                , extra_keyval={ "id_iconography": np.nan, "id_directory": np.nan })

             # place joins
             .build_relationship( "df_place"
                                , "df_r_address_place"
                                , df_from_col="id_address"
                                , df_rel_from="id_place"
                                , df_rel_to="id_address")
        )

        # *********************** WRITING *********************** #
        self.float2int().verifiers().writers()
        return self


    def retype2list(self):
        def tolist(col:SType, dfname) -> SType:
            """
            retype a column to list if it contains only
            list items and `nan` values (but don't retype
            to list if the whole row is `nan`)
            """
            if ( col.loc[ col.notna() ].astype(str).str.startswith("[").all()
                 and not col.isna().all() ):
                return col.fillna("[]").apply(ast.literal_eval)
            else:
                return col

        toretype = [ k for k,v in self.__dict__.items()          # all dataframe objects
                     if k.startswith("df") and v.shape[0] > 0 ]
        for t in toretype:
            df = getattr(self, t)                    # get the dataframe (`self.<df_name>`)
            df = df.apply(tolist, dfname=t, axis=0)  # retype columns to list where necessary
            setattr(self, t, df)                     # update `self.<df_name>`

        return self


    def uuid2int(self):
        """
        so far, all our dataframe indexes are UUIDs. on each dataframe,
        move the UUID to a column named `id_uuid` and replace the index by
        a RangeIndex: in the SQL tables, there will be an INT primary key
        and an UUID for each ressource
        """
        toretype = [ k for k,v in self.__dict__.items()          # all dataframe objects
                     if k.startswith("df") and v.shape[0] > 0 ]
        for t in toretype:
            df = getattr(self, t)                  # get the dataframe (`self.<df_name>`)
            df["id_uuid"] = df.index               # extract the index to a new `id_uuid` column
            df.index = pd.RangeIndex(df.shape[0])  # replace the index by a rangeindex
            setattr(self, t, df)                   # update `self.<df_name>`
        return self


    def foreignkey2int(
        self
        , df_from_name:str
        , df_to_name:str
        , df_from_col:str
    ):
        """
        update all foreign keys from all dataframes so that they point to
        the dataframe's int index, and not to the uuid (see`uuid2int()`)

        :param df_from_name: the name of the `df_from` dataframe (the one
                             with the foreign keys)
        :param df_to_name  : the name of the `df_to` dataframe (the one that
                             the foreign keys point to)
        :param df_from_col : the name of the column containing the foreign
                             keys in the `df_from` dataframe
        """
        def updateforeignkey(cell:str|t.List) -> str|t.List:
            """
            replace the uuid (or array of uuids) in a cell from
            `{df_from}.{df_from_col}` by the proper `int` indexes
            in `df_to`
            """
            # contains a single uuid
            if isinstance(cell, str):
                cell = df_to.loc[ df_to.id_uuid == cell ].squeeze().name
            # contains an array of uuids
            elif isinstance(cell, list):
                cell = [ df_to.loc[ df_to.id_uuid == c ].squeeze().name
                         for c in cell ]

            return cell

        # fetch the dataframes from `self` by their attribute name
        df_from = getattr(self, df_from_name)
        df_to = getattr(self, df_to_name)

        # process
        df_from[ df_from_col ] = df_from[ df_from_col ].apply( updateforeignkey )

        # if the column contains scalars, then they will be retyped
        # to floats because of np.NaN values => retype to Int64
        # (pandas datatype that can contain NaN with integers)
        if ( df_from.shape[0] > 0                                           # `df_from` isn't empty
             and not isinstance(df_from[ df_from_col ].iloc[0], list) ):    # `df_from.df_col` contains a scalar
            df_from[ df_from_col ] = df_from[ df_from_col ].astype("Int64")

        # update the dataframes in `self`
        setattr(self, df_from_name, df_from)
        setattr(self, df_to_name, df_to)

        return self


    def build_relationship(
        self
        , df_from_name:str
        , df_rel_name:str
        , df_from_col:str
        , df_rel_from:str
        , df_rel_to:str
        , extra_keyval:t.Dict={}
        , do_ismain:bool=False
        , start_from:int=0
    ):
        """
        create a relationship table between two dataframes
        (`df_from` and `df_to`, which isn't used in this function):

        * `df_from` has a column `df_from_col` containing in each cell
          an array of uuids pointing to `df_to`. we use `df_from_col` to
          build `df_rel`, a relationship table between `df_from` and `df_to`.
        * `df_rel` has at least two columns:
          * `df_rel_from` : the column with the foreign key to `df_from`
          * `df_rel_to`   : the column with the foreign key to `df_to`
        * `df_rel` can have extra columns specified as a dict using the
          `extra_keyvals` parameter: keys are columns and values are the
          values for each row. `extra_keysvals` will insert the same value
          for the whole column
        * `do_ismain` adds a boolean column named `ismain` to `df_rel`.
          `df_from.df_from_col` contains an array of foreign keys. is `do_ismain`
          is true, the `df_rel.ismain` column will have `True` for the first item
          of the array, `False` otherwise. only used on df_r_iconography_actor
        * (not implemented): when multiple columns of `df_from` point to
          the same `df_to`, use `start_from`

        :param df_from_name : the name of the dataframe containing the array of foreign keys
        :param df_rel_name  : the name relationship table
        :param df_from_col  : the name of the column containing the array of
                              foreign keys in `df_from`
        :param df_rel_from  : the column of `df_rel` containing a foreign key
                              pointing to `df_from`
        :param df_rel_to    : the column of `df_rel` containing a foreign key
                              pointing to `df_to`
        :param extra_keyval : other data inserted as columns to `df_rel`
        :param start_from   : number to start the indexing from. useful when
                              multiple columns of a table point to the same table
        :param do_ismain    : when working on list columns, if True, add and populate
                              the column `df_rel_.ismain`
        """
        def build_list_df_rel(ro:SType) -> SType:
            """
            build `list_df_rel`
            :param ro: the row of `df_from[df_from_col]` being processed
            """
            for i,k in enumerate(ro[df_from_col]):
                dict_ro = { "id_uuid": build_uuid(), df_rel_from: ro.name, df_rel_to: k }
                if len(extra_keyval.keys()) > 0:
                    dict_ro.update(extra_keyval)
                if do_ismain and i==0:
                    dict_ro.update({ "ismain": True })
                elif do_ismain and i!=0:
                    dict_ro.update({ "ismain": False })
                list_df_rel.append(dict_ro)
            return ro

        # fetch the dataframes from `self` by their attribute name
        df_from     = getattr(self, df_from_name)
        df_rel      = getattr(self, df_rel_name)
        list_df_rel = []  # list from which the df will be build. 1 list item = 1 row

        # create `df_rel_populated`
        df_from.loc[ df_from[df_from_col].notna() ].apply( build_list_df_rel, axis=1 )    # populate `list_df_rel`
        index = pd.Index([ _ + df_rel.shape[0] for _ in range(len(list_df_rel)) ])        # avoid duplicate index vals by starting the index of `df_rel_populated` from the last index in `df_rel`
        df_rel_populated = (pd.DataFrame( data=list_df_rel, index=index )                 # transform `list_df_rel` in `df_rel_populated` if it is not empty
                              .rename_axis("index")
                            if not len(list_df_rel) == 0
                            else pd.DataFrame( columns=[ "id_uuid", df_rel_from, df_rel_to ] # if `list_df_rel` is empty, create an empty dataframe with the proper columns
                                                       + list(extra_keyval.keys()) )
                                   .rename_axis("index")
        )

        # concatenate `df_rel_populated` with `df_rel`
        if not df_rel.empty and not df_rel_populated.empty:                               # the if...elif avoids a futurewarning that is raised when concatenating empty dataframes
            df_rel = pd.concat([ df_rel, df_rel_populated ]
                              , axis=0)
        elif df_rel.empty:
            df_rel = df_rel_populated
        df_from = df_from.drop(columns=[df_from_col])                                     # drop the foreign key column on `df_from`

        # update the dataframes in `self`
        setattr(self, df_from_name, df_from)
        setattr(self, df_rel_name, df_rel)

        return self


    def float2int(self):
        """
        columns that contain foreign keys and np.nan values are converted
        to `float64` because the `int` dtype cannot hold NaN values.
        => convert those columns to pandas's `Int64`, which can hold NaN
        values with ints
        """
        for k,v in self.__dict__.items():
            if re.search("^df", k):
                df = v.copy()
                toretype = df.select_dtypes(include="float64").columns
                df[toretype] = df[toretype].astype("Int64")
                setattr(self, k, df)
        return self


    def verifiers(self):
        """
        verify the constistency of the data defined here
        to that in `DATASETS` and `orm`. this forces our dfs
        to have the same structure as that defined on `DATASETS`
        and `orm`, avoiding errors down the way
        """
        def orm2cols(orm_obj) -> t.List[str]:
            """
            extract the columns from the orm class
            `orm_obj` corresponding to an SQL table
            """
            return [ _.name for _ in orm_obj.__table__.columns]

        def assert_same_cols( cols_from:t.List[str]
                            , cols_against:t.List[str]
                            , from_name:str
                            , against_name:str ) -> None:
            """
            check that the columns in `cols_from` are the same
            as the cols in `cols_against`. if not, generate an
            error message showing a comparison between the cols
            in `cols_from` and the cols in `cols_against`

            :param cols_from    : the dataframe columns
            :param from_name    : a name corresponding to `cols_from` (i.e., DF name)
            :param cols_against : the column against which to check
            :param against_name : a name corresponding to `cols_against` (i.e., the
                                  name of the object against which we're checking)
            """
            # build the error message
            from2against = [ _ for _ in cols_against if _ not in cols_from ]  # columns in `cols_against` not in `cols_from`
            against2from = list( set(cols_from).difference(cols_against) )    # columns in `cols_from` not in `cols_against`
            errmsg = ( f"RelationshipProcesses.verifiers()`: dataframe columns do not "
                     + f"correspond to `{against_name}` columns on dataframe `{k}` !\n"
                     )#+ f"* {from_name} columns : `{ sorted(cols_from) }`\n"
                     #+ f"* {against_name} columns  : `{ sorted(cols_against) }`\n")
            if len(from2against):
                errmsg += f"* columns in `{against_name}` that are not in `{from_name}`: {from2against}\n"
            if len(against2from):
                errmsg += f"* columns in `{from_name}` that are not in `{against_name}` : {against2from}\n"

            # run the assertion
            assert all( x==y for x,y in zip( sorted(cols_from), sorted(cols_against) ) ) \
                   and len(cols_from) == len(cols_against), \
                   errmsg

            return

        print("* asserting the validity of our data structures")

        # assert that dataframe columns correspond to
        # the `H_`  variables in `..utils.constants`
        for k,v in self.mapper2datasets.items():
            df = getattr(self, k)
            df_cols = df.columns.to_list()
            ds_cols = [ d for d in DATASETS[v][0] if d!="index" ] # DO NOT USE `.REMOVE()` OR `.POP()` IT COMPLETELY MESSES STUFF UP

            # check that all our dataframes have unique indexes
            # (can be caused by `build_relationship()`)
            assert len(df.index.to_list()) == len(df.index.unique()) \
                   , (f"duplicate indexes found in `{k}` ! check "
                      +"`processes_data2csv.relationships.build_relationship()` for errors !")

            # the columns that we have in `df_cols` are the same as the
            # columns we expect in `DATASETS[v][0]`
            assert_same_cols(df_cols, ds_cols, k, f"DATASETS.{v}")

        # assert that dataframe columns correspond to the
        # columns of our SQL tables as defined in our ORM
        for k,v in self.mapper2orm.items():
            df = getattr(self, k)
            df_cols = df.columns.to_list()
            orm_cols = [ o for o in orm2cols(v) if o!="id" ]  # DO NOT USE `.REMOVE()` OR `.POP()` IT COMPLETELY MESSES STUFF UP
            assert_same_cols(df_cols, orm_cols, k, f"orm.{v.__name__}")

        # uncomment to print a sample of all dataframes
        # sz = os.get_terminal_size().columns
        # for k,v in self.mapper2datasets.items():
        #     df = getattr(self, k)
        #     stars = "".join("*" for _ in range( (sz - (len(k) + 2)) // 2 ) )
        #     print(f"{stars} {k} {stars}")
        #     print("")
        #     print(df.sample(n=5) if df.shape[0] >= 5 else df)
        #     print("")
        #
        #    # uncomment for extra debug prints
        #    print(k)
        #    print(df.columns)
        #    print("")
        return self


    def writers(self):
        """
        write all modified dataframes to file. we use
        a dict and looper to make things quicker
        """
        # dict of `{ <attribute name>: <DATASETS key> }`
        for k,v in self.mapper2datasets.items():
            df = getattr(self, k)
            write_df(v, df, customheader=True, append=False)

        return self


