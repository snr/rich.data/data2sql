from pandas.core.frame import DataFrame as DFType
from typing import Callable
from tqdm import tqdm


class SharedProcesses():
    """
    parent class for all `*Processes` classes. this allows
    to decorate the `df.pipe()` and `df.apply()` functions
    from pandas, while accessing the `self` object 
    (`CartographyProcesses`...). the only downside is that
    the `df` object needs to be passed explicitly to `._pipe()`
    and `._apply()`.
    """ 
    def __init__(self):
        """
        """
    
    def _progress_apply(
            self
            , func:Callable
            , axis:int=1
            , instancefunc:bool=False
            , **kwargs
        ):
        """
        wrapper for `df.progress_apply` that allows us to pipe 
        applies with an `IconographyProcesses` object.
        
        currently, the `**kwargs` are all passed to `func`, and 
        cannot be used as arguments of `.pipe`
        
        :param func: the function to pass
        :param axis: the axis of the `df.apply`
        :param instancefunc: wether the function is an instance function 
                             (in a class, with `self`) or not
        :param kwargs:any other arguments to be passed to `df.apply`
        """
        tqdm.pandas()
        if instancefunc:
            self.df = self.df.progress_apply(lambda x: func(self, x, **kwargs), axis=axis)
        else:
            self.df = self.df.progress_apply(func, axis=axis, **kwargs)
        return self
    
    
    def _pipe(self, func:Callable, **kwargs):
        """
        wrapper for `df.pipe` that allows us to pipe `.pipe`
        with an `IconographyProcesses` object.
        
        since `df.pipe()` passes the `df` to the inner function,
        the dataframe NEEDS TO BE PASSED EXPLICITLY to `func`,
        even if it may be accessed through the `self`, in an
        instance function.
        
        currently, the `**kwargs` are all passed to `func`, and 
        cannot be used as arguments of `.pipe`
        
        :param func: the function to pass
        :param instancefunc: wether the function is an instance function 
                             (in a class, with `self`) or not
        :param kwargs: any other arguments to be passed to `df.pipe`
        """
        # print(" self         :", self, "\n"
        #       , "func         :", func, "\n"
        #       , "kwargs       :", kwargs, "\n"
        #       , "instancefunc :", instancefunc, "\n")
        self.df = self.df.pipe(func, **kwargs)
        return self

    
    
    
    
    
    