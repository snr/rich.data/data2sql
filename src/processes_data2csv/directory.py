from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
# from multiprocessing import Pool, Manager
from unidecode import unidecode
from statistics import mode
import rapidfuzz as rf
from tqdm import tqdm
import pandas as pd
import numpy as np
import re

from ..utils.strings import build_uuid, abv2full, clustering
from ..utils.io import read_df, write_df
from ..utils.constants import DATASETS
from .shared import SharedProcesses


# **************************************************
# transform `in/directory.csv` the `directory` and
# `address` tables
# **************************************************


class DirectoryProcesses(SharedProcesses):
    """
    all processes to transform the directory dataset into SQL tables
    """
    def __init__(self, samplemode:str="sample"):
        """
        :param samplemode: one of full/empty/sample, defines the size of the
            DF that will be processed. see directory2df for more info
        """
        if samplemode not in ["empty", "sample", "full"]:
            raise ValueError(f"DirectoryProcess.pipeline(): `samplemode` must be one of ['empty','sample','full'], got: `{samplemode}`")

        self.df = pd.read_csv( DATASETS["in_directory"][1]
                             , sep="\t", quotechar='"', header=0
                             , names=DATASETS["in_directory"][0]
                             , skip_blank_lines=True , index_col=False
                             , memory_map=True )
        self.df_ins          = read_df("institution", useindexcol=0, customheader=False)
        self.df_admin_person = read_df("admin_person", useindexcol=0, customheader=False)
        self.df_licence      = read_df("licence", useindexcol=0, customheader=False)
        self.df_addr         = None  # will be defined in directory2csv
        self.samplemode      = samplemode  # to set the sample size of the dataframe (0 rows, 0.1% of the whole DF, 100% of the df)

        return


    def pipeline(self):
        """
        pipeline to procsess the directory dataset
        """
        self = self._pipe( self.directory2df
                         , samplemode=self.samplemode
                         , verifier=0 )
        return self

    def directory2df(
            self
            , df:DFType
            , samplemode:str
            , verifier:bool=False
        ) -> DFType:
        """
        process the directory CSV. this csv is very large
        (several million rows), so we can't use the same
        readers/writers as we did for the other csvs.

        from this directory csv, 2 tables are created:
        * `address`
        * `directory`

        the global logic is:
        * we start with one `df` variable that holds the entire directory csv
        * we end with two dfs: `df` (directory.csv, the processed input csv)
          + `self.df_addr` (address.csv, a csv of addresses), with a foreign key
          in `df` that points to `self.df_addr`.
        * we write both dfs to a csv in the end.
        * if `verifier==True`, some prints will help to check that the proper
          foreign key was added to `df`
        * we avoid using huge dataframes for performance reasons,
          so instead we work with specific dicts containing data that can
          be accessed as quickly as possible and we clusterize the dicts.
          we avoid loops as much as possible for performance issues.
        * from that, we'll create dataframes at the end.

        because the directory df is HUGE and it contains a lot of errors, there
        are several options to process it, defined by the parameter `sampemode`.
        * samplemode="full" => process the whole DF (never been tested on the full
          pipeline, takes too long)
        * samplemode="sample" => process a fraction of the DF (4000 rows)
        * samplemode="empty" => simply write empty dataframes with the proper
          structure to a file. this is if we don't want to insert any rows from
          `directory` to the database (which is what we'll do, since the directory table
          is just too dirty). in that case, the whole pipeline isn't run.

        about pandas optimisation:
        * official doc: https://pandas.pydata.org/pandas-docs/stable/user_guide/scale.html
        * fastest way to append to df: https://stackoverflow.com/questions/57000903/what-is-the-fastest-and-most-efficient-way-to-append-rows-to-a-dataframe
        * fast fuzzy string matching with rapidfuzz: https://stackoverflow.com/questions/52631291/vectorizing-or-speeding-up-fuzzywuzzy-string-matching-on-pandas-column
        * rapidfuzz doc: https://maxbachmann.github.io/RapidFuzz/

        :param sample:   process a subset of the dataframe instead of the full one (which takes so much time)
        :param verifier: a flag to print extra info to verify the foreign key validity
        :returns: none
        """
        # ******************** DEFINING VARIABLES ******************** #
        tqdm.pandas()

        # ******************** OPTIMIZING + CLEANING ******************** #
        # preparing: deleting all rows with no address, sampling...
        empty_street = df.loc[ df.street.isna() | df.street.isnull()          # rows with no street name or number
                               | df.number.isna() | df.number.isnull() ]
        fulllen = df.shape[0]
        df = df.drop(index=empty_street.index.to_list())                      # drop the rows with invalid address

        # sample the df: select the number of rows to process
        # if samplemode == empty, we just create an empty dataframes
        # with the data structure that we would expect after running
        # the full pipeline. we write those DFs and return
        if samplemode == "empty":
            df = pd.DataFrame(data=[],
                              columns = [ 'id_uuid', 'gallica_ark', 'gallica_row'
                                        , 'entry_name', 'occupation', 'date'
                                        , 'gallica_page', 'tags', 'id_address'
                                        , 'id_institution', 'id_admin_person'
                                        , 'id_licence'])
            self.df_addr = pd.DataFrame(data=[], columns=['address', 'city', 'country', 'date', 'source'])
            write_df("directory", df, customheader=False, append=False)
            write_df("address", self.df_addr, customheader=False, append=False)
            return

        elif samplemode == "full":
            df = df
        elif samplemode == "sample":
            df = df.sample(frac=0.001)

        df = df.rename(index={ i:build_uuid() for i in df.index.to_list() })   # replace indexes to uuids
        df_verifier = df.sample(100)                                           # to check that the proper directory/address mappings have been created. empty if `samplemode` == "empty", since our main `df` will be empty too
        print(
            f"* complete dataset size               : { fulllen }\n"
            f"* processed dataset size              : { df.shape[0] }\n"
            f"* deleted rows with invalid addresses : { len(empty_street.index.to_list()) } out of { fulllen } rows\n"
        )

        # memory optimisations
        df[["gallica_row", "date", "gallica_page"]] = (df[["gallica_row", "date", "gallica_page"]]
                                                 .apply(pd.to_numeric, downcast="integer"))
        df.gallica_row = df.gallica_row.astype("category")
        df.date = df.date.astype("category")
        df.gallica_page = df.gallica_page.astype("category")

        # cleaning: remove accents from uppercase letters
        # + remove non french accents and correct other ocr errors
        # + remove dots at the end of street numbers
        tqdm.pandas(desc="cleaning street names")
        rgx = re.compile("""(
            ^\s*?[,;=#%&!_\(\)\[\]\.\+\*\"\|\-\']+\s*?
            |\s*?[,;=#%&!_\(\)\[\]\.\+\*\"\|\-\']+\s*?$
            |\s*?\|+\s*?
        )""", re.VERBOSE)
        df.street = (
            df.street
              .str.replace("$", "S")
              .str.replace(rgx, "", regex=True)
              .progress_apply(
                  lambda x: "".join(
                      unidecode(c)
                      if (c.isupper() or c not in ["à", "â", "æ", "ç", "é", "è", "ê", "ë", "î", "ï", "ô", "œ", "ù", "û", "ü", "ÿ"])
                      else c
                      for c in abv2full(x)
                    )
                    if not pd.isna(x) else x
              ).str.strip()
        )
        df.number = (df.number.str.replace("(?<=\d)\s*\.(?!\d)", "", regex=True)
                              .str.replace("\.\s*$", "", regex=True))

        # ******************** CLUSTERING ******************** #
        # prepare clustering: simplify strings + create `names` and `namesmap`
        street = set( df.street.unique().tolist() )  # array of unique street names
        streetsimp = []                              # array of unique simplified street names
        streetmap = {}                               # dict of `{full: simplified}` street names, later changed to `{simplified: full}`
        rgx = re.compile("""
            (\s+?|^)
            (
                (l'|le|la|les|d'|de|du|des|et|à|au|aux)
                |\s
            )+
            (\s+?|$)
        """, flags=re.VERBOSE)
        for s in tqdm(street
                      , total=len(street)
                      , desc="simplifying street names" ):
            simp = rgx.sub( " ", rf.utils.default_process(s) ).replace( " ", "" )
            streetmap[s] = simp
            streetsimp.append(simp)
        streetsimp = set(streetsimp)

        df.street = df.street.map({ k:v for k,v in streetmap.items() })  # replace full street names by non-canonical simplified street names
        streetmap = { v:k for k,v in streetmap.items() }                 # `streetmap` is now a dict of `{simplified: full}` names

        # create clusters by calculating similarities between all simplified names
        # single threaded option
        clusters = []  # variable to store our street name clusters: [ [<array of similar street names>], [<array of similar street names>], ... ]
        for s in tqdm(streetsimp
                      , total=len(streetsimp)
                      , desc="clustering street names" ):
            # only calculate the cluster if it hasn't been added to `clusters` aldready
            if ( len(clusters) == 0 or not any(s in c for c in clusters) ):
                clusters.append(clustering(s, streetsimp))

        # multithreaded equivalent that is actually not faster
        # clusters = Manager().list([])            # variable to store our street name clusters: [ [<array of similar street names>], [<array of similar street names>], ... ]. stored as a `multiprocessing.Manager().list` because it needs to be shared between processes
        # cpucount = os.cpu_count()-1 if os.cpu_count() > 1 else 1
        # p = Pool( processes=cpucount
        #           , initializer=initpool
        #           , initargs=(clusters,) )
        # clusters = list(tqdm(
        #     p.imap(poolworker, [ [s, streetsimp] for s in streetsimp ])
        #     , desc=f"clustering street names ({ cpucount } separate processes)"
        #     , total=len(streetsimp)
        # ))[-1]
        # p.close()

        # create useful dicts
        # clusters: { <simplified mode for the street name>: [
        #     [<cluster of simplified street names>]
        #     , <full version of the mode street name>
        # ] }
        clusters = { mode(c): [ c, streetmap[mode(c)] ]
                     for c in clusters if len(c) > 0 }
        # dict of `{ <non canonical simplified street name>: <canonical simplified street name> }`
        simpmap = {}
        for k in clusters.keys():
            for c in clusters[k][0]:
                simpmap[c] = k
        del streetsimp, streetmap

        # update the `directory` dataframe: replace non-canonical
        # street name by the canonical full street name (so, the final street name)
        df.street = (df.street.map(simpmap, na_action="ignore")                                      # replace by the simplified mode
                              .progress_apply( lambda x: clusters[x][1] if not pd.isna(x) else ""))  # replace by the non-simplified mode
        del simpmap

        # very very rarely (less than 1 in 2000 times), the above
        # map creates NaN street addresses => remove them
        empty_street = df.loc[ df.street.isna() | df.street.isnull() ]
        if empty_street.shape[0] > 0:
            df = df.drop(index=empty_street.index.to_list())
            print(f"deleted another { empty_street.shape[0] } row with invalid address(es)")

        # concatenate the `df.street` and `df.number` columns to a new column, `df.address`
        df["address"] = df.number.astype(str) + ", " + df.street.astype(str)
        df.address = df.address.str.replace("^,\s+", "", regex=True).str.strip()  # if `df.number` is nan, then the separator is useless. remove it.

        # create the `address` dict, source of the `self.df_addr` dataframe
        tqdm.pandas(desc="preparing address dataframe")
        address = { "address": [], "date": [] }  # dict that will be turned into a df
        df.address.progress_apply(lambda x: address["address"].append(x))
        df.date.astype(str).progress_apply( lambda x: address["date"].append(x) )  # retyping categorical type to str is necessary

        # create `df_addr` + deduplicate addresses by
        # extracting year ranges for which an address exists
        tqdm.pandas(desc="grouping addresses")
        self.df_addr = pd.DataFrame( data=address )
        self.df_addr = (self.df_addr.groupby(by=["address"])
                                    .date
                                    .agg(_min="min", _max="max")  # `agg(new_colname0="function0", new_colname1="function1")`; this syntax allows us to specify the column name and to avoid using the defaults `min` and `max` names, adready assigned to a function. yes it's necessary.
                                    .reset_index()
        )
        self.df_addr["source"] = "directory"           #TODO: more precise source description
        self.df_addr = self.df_addr.drop_duplicates()
        self.df_addr["date"] = self.df_addr.progress_apply( lambda x: [x._min, x._max], axis=1 )
        self.df_addr = (self.df_addr.drop( columns=["_min", "_max"] )
                                    .rename(index={ i:build_uuid() for i in self.df_addr.index.to_list() })
        )

        # update `df`: replace the `street` and `number`
        # columns by a foreign key pointing to `self.df_addr`
        dfm = (df.reset_index(names="df_id")                                        # reset the index by a numerical one + move the uuid index to a column named `df_id`
                 .merge( self.df_addr[["address"]].reset_index(names="df_addr_id")  # merge with a dataframe of 2 columns: `address` (df_addr.address) and `df_addr_id` (the uuids of `df_addr`)
                       , how="left"
                       , on=["address"] )
                 .drop_duplicates("df_id")                                          # remove duplicate rows
                 .set_index("df_id")                                                # revert `df`'s index back to the uuid one.
        )
        df["id_address"] = dfm.df_addr_id
        df = df.drop(columns=["number", "street", "address"])
        del dfm

        # create foreign key columns that will later be transformed into relationship tables
        df["id_institution"] = (self.df_ins.loc[ self.df_ins.entry_name == "Bibliothèque nationale de France" ]
                                           .index[0])      # institution uuid
        df.id_institution = df.id_institution.str.split()  # retype scalar to single array list to build the `r_institution` relationship table
        id_admin_person = (self.df_admin_person.loc[ self.df_admin_person
                                                         .last_name
                                                         .isin(["Di Leonardo", "Barman", "Descombes", "Kramer", "Annapureddy"]) ]
                                               .index
                                               .to_list())  # admin persons' uuids
        df = df.assign(id_admin_person=[ id_admin_person for _ in range(df.shape[0]) ])

        # create other necessary columnss in the dfs + do other small changes
        date_arr = [ [x,x] for x in df.date.to_list() ]
        df = df.assign(date=date_arr)                      # retype integer to [ <min>, <max> ] integer array
        df["id_licence"] = (self.df_licence.loc[ self.df_licence.entry_name == "CC BY 4.0" ]
                                           .squeeze()
                                           .name)

        self.df_addr["country"] = "France"
        self.df_addr["city"] = "Paris"

        # check that the proper foreign key has been inserted
        if verifier:
            print(
                "verifying the validity of joins on the `directory` and `address` datasets\n"
                "*************************************************************************\n"
            )
            df_verifier.apply(lambda x: print(
                "entry: "
                , x.entry_name
                , "|"
                , df.loc[ df.entry_name == x.entry_name ].iloc[0].entry_name
                , "\n"
                , "       pre-processing address  :"
                , f"{x.number }, "
                , x.street
                , "\n"
                , "       post-processing address :"
                , self.df_addr.loc[ df.loc[df.entry_name==x.entry_name].id_address ].iloc[0].address
                , "\n"
            ), axis=1)

        # ******************** DATAFRAME WRITING ******************** #
        write_df("directory", df, customheader=False, append=False)
        write_df("address", self.df_addr, customheader=False, append=False)

        return df


