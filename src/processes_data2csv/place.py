from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
import pandas as pd
import numpy as np
import ast

from ..utils.functions import cell2address
from ..utils.io import read_df, write_df
from ..utils.constants import DATASETS
from ..utils.strings import build_uuid
from .shared import SharedProcesses


# **************************************************
# transform `in/place.csv` the `place` and
# `address` tables
# **************************************************


class PlaceProcesses(SharedProcesses):
    """
    all processes to transform the place dataset
    into SQL tables, in a single class for more readability
    """
    def __init__(self):
        self.df = read_df("in_place", useindexcol=0, customheader=True)
        self.df_addr = pd.DataFrame(columns=DATASETS["address"][0][1:])  # empty dataframe
        self.df_addr.index.name = "index"

    def pipeline(self):
        """
        pipeline to process the place dataset
        """
        self.df = (self.df.dropna(how="all")
                          .replace("^(\s|\n)*$", np.nan, regex=True)
                          .drop_duplicates(keep="last")
        )
        self.df.id_richelieu = self.df.id_richelieu.str.strip()
        self = self._pipe(self.place2csv)

        write_df("place", self.df, customheader=False, append=False)
        write_df("address", self.df_addr, customheader=False, append=False)

        return self

    def place2csv(self, df:DFType) -> DFType:
        """
        write the `address.csv` table containing addresses
        by extracting data from `self.df`

        columns to process:
        * 'current'
        * 'atlas1860'
        * 'plot1820'
        * 'vasserot'

        the info in those rows is replaced by an id from the
        `address` table in order to build an external key

        :param df: the place dataframe
        :returns: the updated dataframe
        """
        # ********************* PROCESS THE `PLACE` DATASET ********************* #
        # names of columns to process
        colnames = [
            "current"
            , "atlas1860"
            , "plot1820"
            , "vasserot"
        ]
        # mapper of { <column name>: <date range for that column> }
        colnames_dates = {
            "current":     [ 1900, 2100 ]
            , "atlas1860": [ 1860, 1899 ]
            , "plot1820":  [ 1820, 1859 ]
            , "vasserot":  [ 1810, 1836 ]
        }

        # create the `self.df_addr` dataframe, with, for each column in `df.colnames`,
        dict_addr = {}  # the address parsed and expanded in several columns.
        def build_dict_addr(ro:SType) -> SType:
            """
            build `dict_addr`, which will be transformed into `self.df_addr`
            """
            dict_addr[build_uuid()] = cell2address(cell=ro[col], srccol=col, srcuuid=ro.name)
            return

        for col in colnames:
            df.loc[ df[col].notna() ].apply(build_dict_addr, axis=1)

        self.df_addr         = pd.DataFrame.from_dict(data=dict_addr, orient="index")
        self.df_addr.address = self.df_addr.address.str.strip()
        self.df_addr.date    = self.df_addr.date.astype(str)                 # necessary for `.drop_duplicates()`
        self.df_addr         = (self.df_addr
                                    .loc[ self.df_addr["address"].notna() ]  # drop all rows with no street
                                    .drop_duplicates(keep="last"))
        self.df_addr.date    = self.df_addr.date.map(ast.literal_eval)       # retype to list

        # replace the address in `df` by the uuid in `self.df_addr._index`:
        # `get_foreign_key`: for each primary key of `df`, fetch a foreign key in `self.df_addr` where `self.df_addr.id==id_uuid`
        get_foreign_key = lambda id_uuid: (self.df_addr
                                               .loc[ self.df_addr["srcuuid"] == id_uuid, "_index" ]
                                               .to_list() )
        self.df_addr = self.df_addr.assign( _index=self.df_addr.index.to_list() )  # copy the index to a column named `_index`. assign allows to copy a column without triggering a `SettingWithCopyWarning`
        df = df.assign( id_address=df.index.to_list() )  # store the primary key of `df` to `df.id_address`. then we will replace this key with a foreign key pointing to `df_addr`.
        df.id_address = df.id_address.apply(get_foreign_key)

        # create the date column with a date range for that place
        df["date"] = (df.apply(lambda x: [ _
                                           for c in colnames
                                           if not pd.isna(x[c])
                                           for _ in colnames_dates[c] ], axis=1)  # array of all min,max dates from all columns in `colnames` that are not nan
                        .apply(lambda x: [ min(x), max(x) ]                       # extract `[ <min date>, <max date> ]` from the array
                                         if len(x) > 0
                                         else [ 1900,2100 ]) )

        # create geometry columns that will be populated in `.geometry.py`
        df[["crs_epsg", "vector", "vector_source", "centroid"]] = np.nan

        # create an empty `id_place_group` column
        df["id_place_group"] = np.nan

        # finish the cleaning: remove useless columns
        self.df_addr = self.df_addr.drop(columns=["_index", "srcuuid"])
        df = df.drop(columns=["current", "atlas1860", "plot1820", "vasserot"])

        # ********************* MERGE `self.df_addr` W/ ADDRESSES FROM `directory.csv` ********************* #
        # we should cluster the addresses in `self.df_addr` with the addresses
        # created in `directory2csv`.
        #
        # given * `self.df_addr` the addresses derived from `place.csv`
        #       * `self.df_addr_d` the addresses derived from `directory.csv`,
        #       * `df` the place dataset after processing
        # addresses from `self.df_addr_d` should be added to places defined in `df`.
        # clustering addresses from `self.df_addr` and `self.df_addr_d` by similarity
        # doesn't make sense: the sources differ between the two datasets.
        # 2 possible clustering methods:
        # * if the address is the same in all `self.df_addr` is always the same in
        #   for a single place, then find the same address in `self.df_addr_d` and
        #   add it to the proper place in `df`
        # * else, filter by dates: if the address in `self.df_addr_d` is the same as
        #   an address in `self.df_addr` and the date of the source of an address
        #   in `self.df_addr` and the source of the date in `self.df_addr_d` is in
        #   `self.df_addr.date`, do the clustering.
        #
        # for now, we don't do any clustering since the data in `self.df_addr` is too
        # messy. we just append `self.df_addr` to what's aldready in `address.csv`,
        # add some info and write
        self.df_addr = pd.concat([ read_df("address", useindexcol=0, customheader=False)
                                 , self.df_addr ], axis=0)

        self.df_addr["city"]    = "Paris"
        self.df_addr["country"] = "France"

        id_addresses = df.id_address.explode().loc[ df.id_address.explode().notna() ]  # all foreign keys in `df.id_address` pointing to `df_addr`
        assert id_addresses.apply(lambda x: x in self.df_addr.index.to_list()).eq(True).all(), \
               "some rows in `df_place.id_address` point to unexisting rows in `df_addr`"

        return df



