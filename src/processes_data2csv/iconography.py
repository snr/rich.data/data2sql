from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from unidecode import unidecode
from itertools import chain
from typing import List
import pandas as pd
import numpy as np
import collections
import ast
import re

from ..utils.functions import deduplicate, testforeignkey, dummycol, unnest
from ..utils.strings import build_uuid, isempty, isanon, splitcell, simplify
from ..utils.io import read_df, write_df
from ..utils.constants import DATASETS
from .shared import SharedProcesses


class IconographyProcesses(SharedProcesses):
    """
    all processes to transform the iconography dataset
    into SQL tables, in a single class for more readability.

    most dataframe writing uses a find of convoluted approach,
    where each row is written to a file separately and deduplication
    is done during writing.
    """
    def __init__(self):
        """
        load the data used here: read the necessary dataframes
        """
        self.df              = read_df("in_iconography", useindexcol=0, customheader=True)
        self.df_institution  = read_df("institution", useindexcol=0, customheader=False)
        self.df_licence      = read_df("licence", useindexcol=0, customheader=False)
        self.df_place        = read_df("in_place", useindexcol=0, customheader=True)
        self.df_theme        = pd.DataFrame(data=None, columns=DATASETS["theme"][0][1:])
        self.df_title        = pd.DataFrame(data=None, columns=DATASETS["title"][0][1:])
        self.df_filename     = pd.DataFrame(data=None, columns=DATASETS["filename"][0][1:])
        self.df_named_entity = pd.DataFrame(data=None, columns=DATASETS["named_entity"][0][1:])
        self.df_actor        = pd.DataFrame(data=None, columns=DATASETS["actor"][0][1:])
        return

    def pipeline(self):
        """
        pipeline processing for the `iconography` dataset
        """
        # prepare data
        # self.df_institution.entry_name = self.df_institution.entry_name.apply(simplify)  # the string needs to be simplified in `IconographyProcesses.institution2df`

        # pipeline
        (self._pipe(self.to_list)
             ._pipe(self.deduplicate_cols)
             ._pipe(self.formatcase_cols)
             ._pipe(self.place2uuid)
             ._pipe(self.actor2df)
             ._pipe(self.theme2df)
             ._pipe(self.named_entity2df)
             ._pipe(self.title2df)
             ._pipe(self.filename2df)
             ._pipe(self.institution2df)
             ._pipe(self.admin_person2df)
             ._pipe(self.licence2df)
             ._pipe(self.iconography2df) )
        # write to file
        self.writers()
        return self


    def to_list(self, df_icono:DFType) -> DFType:
        """
        retype columns containing string representation of lists to lists
        + simplify the values contained in those lists (deduplication, lowercase etc.)
        """
        toretype = [ "author_1", "author_2", "date"
                   , "publisher", "filenames", "technique"
                   , "named_entity", "theme", "id_place"]
        tolower = [ "technique", "theme" ]
        formatter = lambda s: (re.sub("\s+", " ", s).strip()  # format the string `s`
                               if isinstance(s, str)
                               else s)
        for t in toretype:
            if t in tolower:
                df_icono[t] = df_icono[t].str.lower()
            df_icono[t] = df_icono[t].fillna("[]")                                      # replace nan values in nan column by the string rpr of an empty list
            df_icono[t] = (df_icono[t].map(ast.literal_eval)
                                      .apply(lambda x: [ formatter(_) for _ in x ])     # simplifiy the string
                                      .apply(lambda x: list(set(x))) )                  # deduplicate list
        return df_icono


    def formatcase_cols(self, df_icono:DFType) -> DFType:
        """
        format the case of the `df_icono.named_entity` column.
        """
        # the columns to format. both are list columns.
        toformat = [ "named_entity" ]
        # format string `s` by making each first character in a word
        # uppercase. the extra regex is to handle english "'s" (else, they will become "'S")
        for t in toformat:
            df_icono[t] = df_icono[t].apply(lambda x: [ re.sub(r"'[sS](?!=\w)", "'s", s.title()) for s in x ])
        return df_icono


    def place2uuid(self, df_icono:DFType) -> DFType:
        """
        replace the foreign key in `df_icono.id_place` from a readable
        place id (stored in `df_place.id_richelieu`) to an uuid
        (`df_place.index`)
        """
        # simplify the important columns
        df_icono.id_place = df_icono.id_place.apply(lambda x: [ s.strip() for s in x])
        self.df_place.id_richelieu = self.df_place.id_richelieu.str.strip()

        # x is a scalar in `df_icono.id_place`. returns a dataframe with all matched items in `self.df_place`
        finder = lambda x: self.df_place.loc[ self.df_place.id_richelieu.eq(x) ]

        df_icono.id_place = (df_icono.id_place
                                     .apply(lambda x: [ finder(_).squeeze().name    # if `_` is valid (points to a single row of `df_place`), get the matching uuid
                                                        for _ in x ]) )             # `_` is a single place id in `df_icono`
        return df_icono


    def deduplicate_cols(self, df_icono:DFType) -> DFType:
        """
        deduplicate columns that should be deduplicated.
        works on both list and string type of columns
        (type checking is done in `functions.deduplicate`).

        `author_1` and `author_2` are deduplicated in `actor2df`
        """
        to_deduplicate = [ "publisher", "theme", "named_entity" ]
        for t in to_deduplicate:
            df_icono[t] = df_icono[t].pipe(deduplicate, colname=f"df_icono.{t}")
        return df_icono


    def actor2df(self, df_icono:DFType) -> DFType:
        """
        create the table `actors`
        """
        def build_actors(s:SType, role:str) -> SType:
            """
            build `self.df_actors` from the data in `s`.

            :param s: the series containing data we want to add to `self.df_actors`
            :param role: the role of the actors described in `s` (publisher, author)
            """
            dict_actors = { build_uuid(): { "entry_name": a }
                            for a in s.explode().unique()
                            if not pd.isna(a) }
            d = pd.DataFrame.from_dict(dict_actors, orient="index")
            d["role"] = role
            self.df_actor = pd.concat([ self.df_actor, d ]
                                      , axis=0)
            return s

        # add data from `author_1`,  `author_2` into `self.df_actor`
        df_icono["authors"] = (df_icono.apply(lambda x: [ x.author_1, x.author_2 ], axis=1)  # [ [<author_1>], [<author_2>] ]
                                       .apply(unnest)                                        # flatten the above list
                                       .pipe(deduplicate, colname="df_icono.authors")        # deduplicate the values
                                       .pipe(build_actors, role="author") )                                 # add data to `self.df_actor`

        # then, add data from `publisher` to `self.df_actor`
        df_icono.publisher = (df_icono.publisher
                                      .pipe(deduplicate, colname="df_icono.publisher")
                                      .pipe(build_actors, role="publisher") )

        # replace values in `df_icono.authors` and
        # `df_icono.publisher` by an UUID of `self.df_actor`
        fetcher = lambda x,y: (self.df_actor.loc[ self.df_actor.entry_name.eq(x)  # x = name of the actor
                                                  & self.df_actor.role.eq(y) ]    # y = their role (author, publisher)
                                            .squeeze()
                                            .name)
        df_icono.authors = df_icono.authors.apply(lambda x: [ fetcher(_, "author")
                                                              for _ in x
                                                              if not pd.isna(_) ])
        df_icono.publisher = df_icono.publisher.apply(lambda x: [ fetcher(_, "publisher")
                                                                  for _ in x
                                                                  if not pd.isna(_) ])

        self.df_actor = self.df_actor.drop(columns=["role"])  # it could also be not-added to `df_actor`, but this raises errors smh

        # test that the join is ok
        for c in [ "authors", "publisher" ]:
            df_icono[c].apply(testforeignkey, df=self.df_actor
                                            , _from="iconography"
                                            , _to="df_actor"
                                            , _fromcol=c)
        return df_icono


    def theme2df(self, df_icono:DFType) -> DFType:
        """
        create the `self.df_theme` table containing
        info on themes. `df_icono.theme` is replaced by an
        array of foreign keys pointing to `df_theme`

        :param df_icono: iconography dataframe
        :returns: the updated `df_icono`
        """
        # build `self.df_theme`
        dict_theme = { build_uuid(): {"entry_name": t, "description": ""}  # will become a df
                       for t in df_icono.theme.explode().unique()
                       if not pd.isna(t) }
        self.df_theme = pd.DataFrame.from_dict(dict_theme, orient="index")  # create `self.df_theme`

        # check that all is good
        assert all([ self.df_theme.entry_name.eq(x).sum() == 1
                     for x in df_icono.theme.explode().unique()
                     if not pd.isna(x) ]), \
               ( "IconographyProcesses.theme2df(): not all values in "
               + "`df_icono.theme` point to a value in `self.df_theme` "
               + "after the creation of `self.df_theme` !" )

        # replace `df_icono.theme` by the corresponding UUID in `self.df_theme`
        fetcher = lambda x: (self.df_theme.loc[ self.df_theme.entry_name.eq(x) ]
                                          .squeeze()
                                          .name)
        df_icono.theme = df_icono.theme.apply(lambda x: [ fetcher(_)
                                                          for _ in x
                                                          if not pd.isna(_) ])
        # test that the join is ok
        df_icono.theme.apply(testforeignkey, df=self.df_theme
                                           , _from="iconography"
                                           , _to="df_theme"
                                           , _fromcol="theme")
        return df_icono


    def named_entity2df(self, df_icono:DFType) -> DFType:
        """
        write the `self.df_named_entity` table containing
        info on named entites. `df_icono.named_entity`
        is replaced by an array of foreign keys pointing
        to `df_theme`

        :param df_icono: iconography dataframe
        :returns: the updated `df_icono`
        """
        #def named_entity2foreignkey(cell:List[str]):
        #    """
        #    build the `dict_named_entity` and replace
        #    contents of `cell` with uuids
        #    """
        #    for n in range(len(cell)):
        #        if not isinstance(cell, list):
        #            raise TypeError(f"`cell` must be of type `list`, got `{type(cell)}` on: `{cell}`")
        #        uuid = build_uuid()
        #        dict_named_entity[uuid] = {
        #            "entry_name": cell[n],
        #            "description": ""
        #        }
        #        cell[n] = uuid
        #    return cell

        # df_icono = dummycol(df_icono, "named_entity", True)  # the column contains no data => create dummy data

        dict_named_entity = { build_uuid(): { "entry_name": n, "description": "" }
                              for n in df_icono.named_entity.explode().unique()
                              if not pd.isna(n) }                                                              # contains data for the future `self.df_named_entity`
        self.df_named_entity = pd.DataFrame.from_dict(dict_named_entity, orient="index")

        # check that all is good
        assert all([ self.df_named_entity.entry_name.eq(x).sum() == 1
                     for x in df_icono.named_entity.explode().unique()
                     if not pd.isna(x) ]), \
               ( "IconographyProcesses.named_entity2df(): not all values in "
               + "`df_icono.named_entity` point to a value in `self.df_named_entity` "
               + "after the creation of `self.df_named_entity` !" )

        # replace `df_icono.theme` by the corresponding UUID in `self.df_theme`
        fetcher = lambda x: (self.df_named_entity
                                 .loc[ self.df_named_entity.entry_name.eq(x) ]
                                 .squeeze()
                                 .name)
        df_icono.named_entity = (df_icono.named_entity
                                         .apply(lambda x: [ fetcher(_)
                                                            for _ in x
                                                            if not pd.isna(_) ]) )
        df_icono.named_entity.apply(testforeignkey, df=self.df_named_entity                       # test that the join is ok
                                            , _from="iconography"
                                            , _to="df_named_entity"
                                            , _fromcol="named_entity")
        return df_icono


    def title2df(self, df_icono:DFType) -> DFType:
        """
        write the `self.df_title` table containing info on titles.

        impacted columns:
        * `title`
        * `title_secondary`

        :param df_icono: the dataframe
        :returns: the updated `df_icono`
        """
        def title2foreignkey(ro:SType) -> SType:
            """
            build the `dict_named_entity`. the join here is from
            the `title` table to `iconography` => the foreign key
            is on `title` and points to the index of `iconography`.
            `df_icono.title` and `df_icono.title_secondary` are not
            updated and will be deleted below

            :param ro: the row to process
            """
            for col in ["title", "title_secondary"]:
                uuid = build_uuid()
                dict_title[uuid] = {
                    "entry_name": ro[col],
                    "ismain": True if col=="title" else False,
                    "id_iconography": ro.name
                }
            return ro
        def ismain_cleaner(ro:SType) -> SType:
            """
            in `df_icono`, in some cases, there is a `title_secondary` but not
            a `title`. in that case, set `ismain` to True
            on the second title
            """
            if ( self.df_title.loc[ self.df_title.id_iconography == ro.name ]  # `ro` is the only row of `df_title` linked to `id_iconography`
                              .shape[0] == 1
                 and ro.ismain == False ):                                     # but `ro` is not the main title
                ro.ismain = True
            return ro

        dict_title = {}
        df_icono = df_icono.apply(lambda x: title2foreignkey(x), axis=1)
        self.df_title = pd.DataFrame.from_dict(dict_title, orient="index")
        self.df_title = self.df_title.dropna(subset=["entry_name"])            # drop nan rows
        self.df_title = self.df_title.apply(ismain_cleaner, axis=1)  # set `ismain` to True if there's only one title for a row of `df_icono`

        # test
        self.df_title.id_iconography.apply(testforeignkey, df=df_icono
                                                         , _from="df_title"
                                                         , _to="df_icono"
                                                         , _fromcol="id_iconography")
        return df_icono


    def filename2df(self, df_icono:DFType) -> DFType:
        """
        write the `self.df_filename` table containing the names of
        files stored locally (that is, by the INHA servers).

        impacted columns from iconography dataset:
        * `filenames`

        this column is dropped, as the join is from
        `self.df_filename.id_iconography` to `df_icono`

        :param df_icono: the iconography dataframe
        :returns: the updated `df_icono`
        """
        def filename2foreignkey(ro:SType) -> SType:
            """
            build `self.df_images`
            """
            for fn in ro.filenames:
                uuid = build_uuid()
                dict_filename[uuid] = {
                    "url": fn,
                    "id_iconography": ro.name,
                    "id_cartography": np.nan,
                    "id_licence": np.nan,
                    "latlngbounds": np.nan
                }
            return ro

        dict_filename = {}
        df_icono = df_icono.apply(filename2foreignkey, axis=1)
        self.df_filename = pd.DataFrame.from_dict(dict_filename, orient="index")

        self.df_filename.id_iconography.apply(testforeignkey, df=df_icono
                                                        , _from="df_filename"
                                                        , _to="df_icono"
                                                        , _fromcol="id_iconography")
        return df_icono


    def institution2df(self, df_icono:DFType) -> DFType:
        """
        source foreign keys for the `institution` column of the
        `iconography` dataset. in `df_icono.institution`, the
        human readable name is replaced by an array of uuids
        pointing to `institution.id`.

        basically: in the step 1_data_preparation, we normalized
        institution notation to the values in `self.df_institution`.
        the only difference is in `df_icono.institution` when we have:
        `Institution de conservation (Institution de rattachement)`.
        in those cases, `Institution de conservation` and
        `Institution de rattachement` will be split in 2 items before
        fetching the two corresponding UUIDs in `self.df_institution`:

        1. `Petit Palais (Paris Musées)`
        2. [Petit Palais, Paris Musées]
        3. [<uuid1>, <uuid2>]

        :param df_icono: the dataframe
        :returns: the updated `df_icono`
        """
        # split `df_icono` into a list of institutions
        # (see steps 1. and 2. from above example)
        df_icono.institution = (df_icono.institution
                                        .str.extract(r"^([^\(]+)(\([^\)]+\))?$")            # splits `institution` into 2 columns: one for the Institution de conservation; the other for the Institution de rattachement (can be NaN): Musée Carnavalet (Paris Musées) is split in 2
                                        .apply(lambda x: [ re.sub("[\(\)]", "", _).strip()  # bring it back into a list + remove nan elts + remove parenthesis + strip (important !!!)
                                                           for _ in x
                                                           if not pd.isna(_) ]
                                              , axis=1) )

        # check our data is valid
        assert all([ self.df_institution.entry_name.str.lower().eq(x.lower()).sum() == 1
                     for x in df_icono.institution.explode().unique()
                     if not pd.isna(x) ]), \
               ("IconographyProcesses.institution2df(): all values of "
               + "`df_icono.institution` must point to a single row of "
               + "`df_institution`!")

        # replace the human readable name by a foreign key
        fetcher = lambda x: (self.df_institution
                                 .loc[ self.df_institution.entry_name.str.lower().eq(x.lower()) ]
                                 .squeeze()
                                 .name)
        df_icono.institution = df_icono.institution.apply(lambda x: [ fetcher(_)
                                                                      for _ in x ])
        # check that we're good
        df_icono.institution.apply(testforeignkey, df=self.df_institution
                                                 , _from="df_icono"
                                                 , _to="df_institution"
                                                 , _fromcol="id_institution")
        return df_icono


    def admin_person2df(self, df_icono:DFType):
        """
        add an `id_admin_person` column containing data on the authors
        of the ressources (on the side of the Richelieu project)

        impacted rows:
        * `id_admin_person`: an array of uuids pointing to `admin_person.id`.
          this will be deleted after the joins are performed

        :param df_icono: the dataframe objects
        :returns: the IconographyProcesses with an updated dataframe
        """
        df_ap = read_df("admin_person", useindexcol=0, customheader=False)
        df_icono["id_admin_person"] = [df_ap.loc[
            df_ap.last_name.isin([ "Duvette"
                                 , "Gain"
                                 , "Dasilva"
                                 , "Baranger"
                                 , "Thiroux"
                                 , "Akgonul" ])
        ].index.to_list()] * df_icono.shape[0]  # `* df_icono.shape[0]`: add the indexes to all rows
        return df_icono


    def licence2df(self, df_icono:DFType) -> DFType:
        """
        add the uuid to a licence on the iconography df_icono
        """
        df_icono["id_licence"] = (self.df_licence
                                      .loc[ self.df_licence.entry_name.eq("CC BY 4.0") ]
                                      .squeeze()
                                      .name)
        return df_icono


    def iconography2df(self, df_icono:DFType) -> DFType:
        """
        finish cleaning the icongraphy table: rename columns,
        drop unnecessary columns...

        :param df_icono: the iconography dataframe
        :returns: the updated `self.df` dataframe
        """
        df_icono = (df_icono.drop( columns=[ "filenames", "title"
                                           , "title_secondary", "commentary"
                                           , "author_1", "author_2" ])
                            .rename({ "institution"         : "id_institution",
                                      "authors"             : "id_author",
                                      "publisher"           : "id_publisher",
                                      "institution"         : "id_institution",
                                      "theme"               : "id_theme",
                                      "named_entity"        : "id_named_entity",
                                      "produced_richelieu"  : "produced",
                                      "represents_richelieu": "represents" }
                                    , axis=1)
        )
        return df_icono


    def writers(self) -> None:
        """
        write all our files to csv
        """
        write_df("iconography", self.df, customheader=False, append=False)
        write_df("actor", self.df_actor, customheader=False, append=False)
        write_df("filename", self.df_filename, customheader=False, append=False)
        write_df("named_entity", self.df_named_entity, customheader=False, append=False)
        write_df("theme", self.df_theme, customheader=False, append=False)
        write_df("title", self.df_title, customheader=False, append=False)

        return None



# **************************************
# UNUSED FUNCTIONS
# **************************************
#    def actor2df(self, df:DFType) -> DFType:
#        """
#        build the actor dataframe:
#        * build `self.df_actor` from `self.df_in_actor`,
#          a handmade index of actors in our dataset
#        * replace the actor name in `df['author_1', 'author_2', 'publisher']`
#          by a foreign key pointing to `self.df_actor`
#
#        !!!!! there is a row in `self.df_actor` for `np.nan`,
#        !!!!! or unknown actor, with a `last_name` "Anonyme".
#        !!!!! when `df.author_1` and `df.author_2`, then `df.author_1`
#        !!!!! is linked to this `Anonyme` row.
#        !!!!! for `author_2` and `publisher`, when a row is `np.nan`,
#        !!!!! no foreign key is inserted
#
#        :param df: the iconography dataframe
#        :returns: the updated iconography dataframe
#        """
#        # *********************** functions *********************** #
#        def act2uuid(act:str, anon:bool) -> str:
#            """
#            `act` is an actor name in the iconography dataframe `df`.
#            replace it by a foregin key pointing to `self.df_actor`, a
#            dataframe with an actor name for each row.
#            `anon` must be true for `df.author_1` only !
#
#            :param act: the simplified actor name, contained in `df.simp`
#            :param anon: if `True`, replace `act` by the uuid pointing to
#                         the `Anonyme` row of `self.df_actor`. else, replace
#                         `act` by `np.nan`, which means that the cell will
#                         point to nothing
#            :returns: the matched uuid in `self.df_actor`
#            """
#            # replace `act` by the uuid pointing to `Anonyme` in `self.df_actor`
#            if anon is True and pd.isna(act):
#                return (self.df_actor.loc[ self.df_actor.first_name.isna()
#                                           & self.df_actor.last_name.eq("Anonyme") ]
#                                     .squeeze()
#                                     .name)
#            # replace `act` by np.nan: no foreign key for that row
#            elif anon is False and pd.isna(act):
#                return np.nan
#            else:
#                # a dataframe with all matched items (`self.df_actor.sources`
#                # contains `act`). can contain 1 row, several rows or no rows
#                m = self.df_actor.loc[ self.df_actor.sources.apply(lambda x: act in x) ]
#
#                # if only 1 row is matched => return its uuid
#                if m.shape[0] == 1:
#                    return m.squeeze().name
#
#                # several rows matched (the `act` string points to several
#                # actors in `df_actor` => return an array of matched uuids
#                elif m.shape[0] > 1:
#                    return m.index.to_list()
#
#                # no rows matched => perform a fuzzier matching and do the
#                # same process (it's necessary because of how the actors index
#                # (`self.df_actors_in`) is created in `1_data_preparation`
#                else:
#                    _act = act
#                    act = "".join(filter(str.isalnum, act)).lower()
#                    m = self.df_actor.loc[ self.df_actor.sources_simp.apply(lambda x: act in x) ]
#                    if m.shape[0] == 1:
#                        return m.squeeze().name
#                    elif m.shape[0] > 1:
#                        return m.index.to_list()
#                    else:
#                        raise ValueError("no actor match could be made in `Iconography.actor2df().act2uuid()` "
#                                         f"`on actor {_act}`, simplified to `{act}`")
#            return
#
#
#
#        # *********************** process *********************** #
#        # do the splits:
#        # * split+explode multivalued columns
#        # * split `df_in_actor` in 2 dfs: one where `first_name` and `last_name` are notna, one where only `last_name` is notna
#        # * extract simplified values
#        df = df.explode("author_1").explode("author_2").explode("publisher")
#        self.df_in_actor.date = self.df_in_actor.date.apply(ast.literal_eval)
#        self.df_in_actor.loc[ self.df_in_actor.last_name.notna(), "last_name" ] = self.df_in_actor.loc[ self.df_in_actor.last_name.notna(), "last_name" ].apply(splitcell)
#        self.df_in_actor = self.df_in_actor.explode("last_name")
#
#        df_ln = self.df_in_actor.loc[ self.df_in_actor.last_name.notna() & self.df_in_actor.first_name.isna() ]     # actors with last name only
#        df_fnln = self.df_in_actor.loc[ self.df_in_actor.last_name.notna() & self.df_in_actor.first_name.notna() ]  # actors with first name + last name
#
#        # sources in all rows where first name and last name are `np.nan`
#        na_src = (self.df_in_actor.loc[ self.df_in_actor.first_name.isna()
#                                        & self.df_in_actor.last_name.isna()
#                                        , "src" ]
#                                  .unique()
#                                  .tolist())
#
#        # create `self.df_actor` from `self.df_in_actors`.
#        # dates are always pretty much the same between different actors grouped together
#        # => extract the first and last date amongst all dates in a df group
#        dfg_ln = df_ln.groupby(by=["last_name"])
#        dfg_fnln = df_fnln.groupby(by=["first_name", "last_name"])
#        dict_df_actor = {}
#        # add nan values to `dict_df_actor`. we give them the name `Anonyme`
#        dict_df_actor[build_uuid()] = { "first_name" : np.nan,
#                                        "last_name"  : "Anonyme",
#                                        "date"       : [],
#                                        "sources"    : na_src }
#        # add the group where `last_name` is notna is `first_name` is
#        for k,i in dfg_ln:
#            dict_df_actor[build_uuid()] = { "first_name" : np.nan,
#                                            "last_name"  : k[0],
#                                            "date"       : [ _
#                                                             for el in i.date.to_list()
#                                                             for _ in el ],
#                                            "sources"    : i.src.tolist() }
#        # add the group where `last_name` and `first_name` are notna
#        for k,i in dfg_fnln:
#            dict_df_actor[build_uuid()] = { "first_name" : k[0],
#                                            "last_name"  : k[1],
#                                            "date"       : [ _
#                                                             for el in i.date.to_list()
#                                                             for _ in el ],
#                                            "sources"    : i.src.to_list() }
#        self.df_actor = pd.DataFrame.from_dict(dict_df_actor, orient="index")
#        self.df_actor.date = self.df_actor.date.apply(lambda x: [ min(x), max(x) ]
#                                                                if len(x) > 0 else x)
#        self.df_actor["sources_simp"] = self.df_actor.sources.apply(lambda x: [ "".join(filter(str.isalnum, el))
#                                                                                  .lower()
#                                                                                for el in x ])
#
#        # simplify sources in `self.df_actor`
#        self.df_actor.sources = (self.df_actor.explode("sources")
#                                              .sources
#                                              .rename_axis("index")
#                                              .str.replace("\s+", " ", regex=True)
#                                              .str.strip()
#                                              .groupby("index")
#                                              .agg(lambda x: x.to_list()))
#
#        # update `df`: replace actor names by uuids in `self.df_actor`
#        for k,v in { "author_1": True, "author_2": False, "publisher": False }.items():
#            df[k] = (df[k].str.replace("\s+", " ", regex=True)
#                          .str.strip()
#                          .apply(act2uuid, anon=v))
#        self.df_actor = self.df_actor.drop(columns=["sources_simp"])
#
#        # "un-explode" `df`
#        # loosely based on: https://stackoverflow.com/questions/64235312/how-to-implodereverse-of-pandas-explode-based-on-a-column
#        df = (df.groupby("index")
#                .agg(lambda x: x.tolist()                                            # unexplode
#                               if x.name in [ "author_1", "author_2", "publisher" ]
#                               else x.iloc[0])
#                .apply(lambda x: [ unnest(_) for _ in x.to_list() ]                  # actor columns contain nested lists (`[uuid, [uuid,uuid], uuid]`) => flatten them
#                                 if x.name in ["author_1", "author_2", "publisher"]
#                                 else x
#                       , axis=0))
#        for c in ["author_1", "author_2", "publisher"]:
#            df[c] = df[c].apply(lambda x: [ _ for _ in x if pd.notna(_) ])  # remove `np.nan` items in lists in all 3 columns
#            df[c] = df[c].apply(lambda x: list(set(x)))                     # remove duplicates inserted because of `explode`
#
#        # finally, drop all rows in `self.df_actor` that point to no
#        # row in `df`: in `1_data_preparation`, author names may be dropped
#        # after the `actors.csv` handmade index is created (there's no way
#        # around this) so some entries of `actors.csv` must be dropped
#        predrop = self.df_actor.shape[0]
#        used = pd.concat([ df.author_1, df.author_2, df.publisher ]).explode().unique()  # all actors in `self.df_actor` referenced by `self.df`
#        self.df_actor = self.df_actor[ self.df_actor.index.isin(used) ]                  # drop unused rows
#        print(f"* dropped { predrop - self.df_actor.shape[0] } unsed rows in `self.df_actor`")
#
#        # test that the join is done correctly.
#        # `filtercol` allows to only test foreign keys that are not nan.
#        for _ in ["author_1", "author_2", "publisher"]:
#            df["filtercol"] = df[_].apply(lambda x: len(x)==0
#                                                    or any(pd.notna(v) for v in x))
#            df.loc[ df.filtercol==True, _ ].apply(testforeignkey
#                                                  , df=self.df_actor
#                                                  , _from="iconography"
#                                                  , _to="df_actor"
#                                                  , _fromcol=_)
#        df = df.drop(columns=["filtercol"])
#
#        return df

    # def actor2df(self, df:DFType) -> DFType:
    #     """
    #     write the `self.df_actor` table containing info on actors.
    #
    #     impacted columns:
    #     * `author_1`
    #     * `author_2`
    #     * `publisher`
    #     the value in those columns is replaced by an array of foreign keys
    #
    #     :param df: iconography dataframe
    #     :returns: the updated `df`
    #     """
    #     def actor2foreignkey(cell:list, notperson:bool=False) -> list:
    #         """
    #         build `self.dict_actor` (which will be transformed
    #         into a `self.df_actor`) and replace the actor names
    #         in `cell` by uuids pointing to values of `dict_actor`
    #
    #         :param notperson: the column doesn't contain data on persons,
    #                           dans `cell2actor` will not be used
    #         """
    #         assert isinstance(cell, list) \
    #                , f"`cell` must be of type `list`, got `{type(cell)}` on: `{cell}`"
    #         for n in range(len(cell)):
    #             actor = cell[n]
    #             uuid = build_uuid()
    #             if notperson:
    #                 actor = {
    #                     "last_name": "Anonyme" if isanon(actor) else actor,
    #                     "first_name": "",
    #                     "date": [],
    #                     "source": actor
    #                 }
    #             else:
    #                 actor = cell2actor(actor)
    #             dict_actor[uuid] = actor
    #             cell[n] = uuid
    #         return cell
    #
    #     dict_actor = {}  # dict that will be turned into the `self.df_actor` dataframe
    #
    #     # build `self.df_actor`
    #     df.author_1 = df.author_1.apply(actor2foreignkey)
    #     df.author_2 = df.author_2.apply(actor2foreignkey)
    #     df.publisher = df.publisher.apply(actor2foreignkey, notperson=True)
    #     self.df_actor = pd.DataFrame.from_dict(dict_actor, orient="index")
    #
    #     # remove duplicates from `df_actor` (using fuzzy string matching)
    #     self.df_actor = self.df_actor.pipe(deduplicate, cols=["first_name", "last_name"]
    #                                        , name="df_actor")
    #
    #     # update the foreign keys in `df` that point to `self.df_actor`
    #     df.author_1 = self.df.author_1.apply(updateforeignkey, df=self.df_actor)
    #     df.author_2 = self.df.author_2.apply(updateforeignkey, df=self.df_actor)
    #     df.publisher = self.df.publisher.apply(updateforeignkey, df=self.df_actor)
    #
    #     self.df_actor = self.df_actor.pipe(dropdeduplicated)
    #
    #     # test that the join is done correctly
    #     for _ in ["author_1", "author_2", "publisher"]:
    #         df[_].apply(testforeignkey
    #                     , df=self.df_actor
    #                     , _from="iconography"
    #                     , _to="df_actor"
    #                     , _fromcol=_)
    #
    #     return df


