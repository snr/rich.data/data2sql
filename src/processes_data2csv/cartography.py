from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from unidecode import unidecode
import geopandas as gpd
import pandas as pd
import numpy as np
import typing as t
import json
import ast
import re

from ..utils.functions import testforeignkey, build_uuid
from ..utils.io import read_df, write_df
from ..utils.strings import simplify
from .shared import SharedProcesses


class CartographyProcesses(SharedProcesses):
    """
    all processes to transform the cartography dataset into SQL
    tables (except for joins and stuff, which will be done later)
    """
    def __init__(self):
        self.df              = read_df("in_cartography", useindexcol=0, customheader=True)
        self.df_filename     = read_df("filename", useindexcol=0, customheader=False)
        self.df_place        = read_df("place", useindexcol=0, customheader=False)
        self.df_institution  = read_df("institution", useindexcol=0, customheader=False)
        self.df_admin_person = read_df("admin_person", useindexcol=0, customheader=False)
        self.df_licence      = read_df("licence", useindexcol=0, customheader=False)

        return


    def pipeline(self):
        """
        pipeline for the processing of the `cartography` dataset
        """
        self = (self._pipe(self.to_list_or_dict)  # ok
                    ._pipe(self.place2df)
                    ._pipe(self.institution2df)
                    ._pipe(self.raster2df)
                    ._pipe(self.licence2df)
                    ._pipe(self.admin_person2df)
                    ._pipe(self.cartography2df)
        )
        # write to file
        self.writers()
        return self


    def to_list_or_dict(self, df_carto:DFType) -> DFType:
        """
        retype the columns in `to_retype` to list or dict,
        based on the type of the data aldready present in the cols.
        """
        # at the moment, df_carto.id_place only contains scalars
        # (1 carto entry is linked to 1 loc). however, our data model
        # allows for a single carto entry to represent several locs
        # => retype `df_carto.id_place` to an array.
        df_carto.id_place = df_carto.id_place.apply(lambda x: [ _.strip().replace(" ", "")
                                                                     for _ in x.split("|") ])

        # retype the other cols + fill them
        to_retype = ["date", "vector", "latlngbounds"]
        for t in to_retype:
            try:
                df_carto[t] = df_carto[t].fillna("[]").apply(ast.literal_eval)
            except ValueError:
                df_carto[t] = df_carto[t].fillna("{}").apply(json.loads)  # vector is a dict => use json.parse

        return df_carto


    def place2df(self, df_carto:DFType) -> DFType:
        """
        complete the `self.df_place` df by adding data from `df_carto`:

        * `df_place.vector` is the default geojson for a place
        * `df_place.vector_source` gives the data source for a polygon
          (vasserot, parcellaire1900...)
        * `df_place.centroid` gives the centroid for a given place
        """
        # before starting, make sure that our data
        # is ok (has been checked before but hey)
        assert all( self.df_place.id_richelieu.isin(df_carto.id_place.explode()) ), \
               (f"Cartography.place2df(): not all values of "
                +"`df_place.id_richelieu` are in `df_carto.id_place`")

        # functions
        def extract_vector(ro:SType) -> SType:
            """
            extract `df_place.vector` and `df_place.vector_source`
            from `df_carto.vector` and `df_carto.map_source` based on the
            place id `ro.id_richelieu`
            """
            id_loc = ro.id_richelieu
            df_carto_filter = (df_carto.explode(column="id_place")
                                       .loc[ df_carto.explode(column="id_place").id_place.eq(id_loc)
                                           , ["vector", "map_source"] ])

            # by default, we try to extract the vector from the `parcellaire1900` source
            mask = df_carto_filter.map_source.eq("parcellaire1900")
            if any(mask):
                out = df_carto_filter.loc[ mask ].squeeze().to_list()

            # if `id_loc` is not present in `parcellaire1900`,
            # it can be represented by 1..n rows (with diffrent map sources)
            # then, we order `map_sources` alphabetically to try to extract the
            # same `map_source` value when possible.
            # the aim is to have as few `df_place.vector_sources` distinct values
            # as possible: we want to avoid mixing map sources in `df_place`.
            else:
                out = (df_carto_filter.sort_values(by="map_source", axis=0)
                                      .iloc[0]
                                      .to_list())
            ro.vector, ro.vector_source = out
            return ro


        def extract_centroid(df:DFType) -> DFType:
            """
            map each row of `df_place` to a centroid
            :param df: the `self.df_place`
            """
            geom = (gpd.GeoDataFrame
                       .from_features([ { "type": "Feature",
                                          "properties": {},
                                          "geometry": v }
                                        for v in df.vector.to_list() ]
                                     , crs=crs_epsg)
                       .to_crs(epsg=3857))  # to calculate centroids, we need to convert epsg:4326 (WGS84 as a coordinate CRS) to epsg:3857 (WGS84 as a projected CRS)
            df.centroid = geom.centroid.to_crs(epsg=crs_epsg).to_list()              # extract centroid, reconverting it to epsg:4326
            df.centroid = df.centroid.apply(lambda c: { "type": "Point",             # convert to geometry to geojson point
                                                        "coordinates": [c.x, c.y] })
            return df

        # populate `df_place` with a vector, a centroid and a granularity
        crs_epsg = df_carto.crs_epsg.unique()[0]
        self.df_place["crs_epsg"] = crs_epsg
        self.df_place[["vector", "vector_source", "centroid"]] = np.nan
        self.df_place = (self.df_place.apply(extract_vector, axis=1)
                                            .pipe(extract_centroid))

        # replace `df_carto.id_place` from `df_place.id_richelieu` to `df_place.index`
        fetcher = lambda x: (self.df_place.loc[ self.df_place.id_richelieu.eq(x) ]
                                             .squeeze()
                                             .name)
        df_carto.id_place = df_carto.id_place.apply(lambda x: [ fetcher(_) for _ in x ])
        return df_carto


    def institution2df(self, df_carto:DFType) -> DFType:
        """
        replace the institution name in `df_carto.institution`
        by the corresponding uuid in `self.df_place.index`.

        this includes retyping rows that contain multiple
        institutions into lists:
        1. `Petit Palais (Paris Musées)`
        2. [Petit Palais, Paris Musées]
        3. [<uuid1>, <uuid2>]
        """
        _df_carto = df_carto.loc[ df_carto.institution.notna() ]  # all rows except those with `source==contemporain`

        # split `df_carto.institution` into a list of institutions
        # (see steps 1. and 2. from above example)
        _df_carto.institution = (_df_carto.institution
                                          .str.extract(r"^([^\(]+)(\([^\)]+\))?$")                        # splits `institution` into 2 columns: one for the Institution de conservation; the other for the Institution de rattachement (can be NaN)
                                          .apply(lambda x: [ _.replace("(", "").replace(")", "").strip()  # bring it back into a list + remove nan elts + remove parenthesis + strip (important !!!)
                                                             for _ in x
                                                             if not pd.isna(_) ]
                                                , axis=1) )

        # replace institutions names by UUIDs
        # raises a warning but whatever
        fetcher = lambda x: (self.df_institution.loc[ self.df_institution.entry_name.eq(x) ]
                                                .squeeze()
                                                .name)
        _df_carto.institution =_df_carto.institution.apply(lambda x: [ fetcher(_) for _ in x ])
        df_carto.update(_df_carto)

        # check that we're good
        df_carto.institution.apply(lambda x: testforeignkey( x
                                                           , df=self.df_institution
                                                           , _from="df_carto"
                                                           , _to="df_institution"
                                                           , _fromcol="id_institution")
                                  if not pd.isna(x) else False)
        return df_carto


    def raster2df(self, df_carto:DFType) -> DFType:
        """
        deport `df_carto.raster` to `self.df_filename` and replace
        the filename to a foreign key pointing to `self.df_filename.index`
        """
        def raster2foreignkey(ro:SType) -> SType:
            """
            build `df_filename` from raster files
            """
            uuid = build_uuid()
            dict_filename[uuid] = { "url"           : ro.raster,
                                "latlngbounds"  : ro.latlngbounds,
                                "id_iconography": np.nan,
                                "id_cartography": ro.name,
                                "id_licence"    : np.nan }
            return uuid

        dict_filename = {}
        df_carto.loc[ df_carto.raster.notna() ].apply(raster2foreignkey, axis=1)
        self.df_filename = pd.concat([ self.df_filename, pd.DataFrame.from_dict(data=dict_filename, orient="index") ]
                                 , axis=0)
        self.df_filename.id_cartography.apply(lambda x: testforeignkey( x
                                                                  , df=df_carto
                                                                  , _from="df_filename"
                                                                  , _to="df_carto"
                                                                  , _fromcol="id_cartography")
                                         if not pd.isna(x) else False)
        return df_carto


    def licence2df(self, df_carto:DFType) -> DFType:
        """
        add licence info to the dataframe
        """
        df_carto["id_licence"] = (self.df_licence
                                      .loc[ self.df_licence.entry_name == "CC BY 4.0" ]
                                      .squeeze()
                                      .name)
        return df_carto


    def admin_person2df(self, df_carto:DFType) -> DFType:
        """
        add uuids of `self.df_admin_person` to `df_carto.id_admin_person`
        """
        uuids = (self.df_admin_person
                     .loc[ (self.df_admin_person.last_name == "Jeanson")
                         | (self.df_admin_person.last_name == "Prudhomme")
                         | (self.df_admin_person.last_name == "Kervegan") ]
                     .index.to_list()
        )
        df_carto["id_admin_person"] = [ uuids for _ in range(df_carto.shape[0]) ]

        df_carto.apply(lambda x: testforeignkey( idx=x.id_admin_person
                                               , df=self.df_admin_person
                                               , _from="df_carto"
                                               , _to="df_admin_person"
                                               , _fromcol="id_admin_person")
                      , axis=1)
        return df_carto


    def cartography2df(self, df_carto:DFType) -> DFType:
        """
        finish cleaning `self.df`: rename/drop columns
        """
        df_carto = (df_carto.rename(columns={ "institution": "id_institution" })
                            .drop(columns=[ "credits", "latlngbounds", "raster", "crs_wkt" ]))
        return df_carto


    def writers(self):
        """
        write all the modified dataframes to csvs
        """
        write_df("cartography", self.df, customheader=False, append=False)
        write_df("filename", self.df_filename, customheader=False, append=False)
        write_df("place", self.df_place, customheader=False, append=False)
        return self



# UNUSED FUNCTIONS AND CODE

# in `pipeline()`: cleaning that is now outdated.
        ## AFAIK, THIS IS NOW USELESS OR CORRESPONDS TO PREVIOUS STATES OF THE DATA
        #self.df_institution.entry_name = (self.df_institution.entry_name.apply(lambda x: unidecode(str(x)))
        #                                                      .str.replace(" ", "")
        #                                                      .str.lower()
        #                                                      .replace("^nan$", np.nan, regex=True))
        #self.df.institution = (self.df.institution.apply(lambda x: unidecode(str(x)))
        #                                          .str.lower()
        #                                          .str.replace(" ", "")
        #                                          .str.replace("^bnf$", "bibliothequenationaledefrance", regex=True)
        #                                          .replace("^nan$", np.nan, regex=True))
        #
        #self.df.loc[ self.df.isfullstreet, "id_place" ] = "RV0"                                                           # associate the full street to `RV0`, the whole "Rue Vivienne"
        #self.df.loc[ self.df.isfullstreet, "id_place" ] = self.df.loc[ self.df.isfullstreet, "id_place" ].str.split()  # `.split()` is needed to retype the string to list

        # # some tests
        # # 1) count number of entries in `df_place` described by at least one
        # #    row of `df` which only describes a single place (aka: we have a
        # #    geojson and an image describing that place only). a row of `df_place`
        # #    can be described by several rows of `df` that describe only that place
        # self.df["singleloc"] = self.df.id_place.apply(lambda x: x[0]            # human-readable place identifier or np.nan: the cartography entry represents a single place
        #                                                            if len(x) == 1
        #                                                            else np.nan)
        # self.df_place["singlecarto"] = ""                                       # not `np.nan` to avoid incompatible dtype warnings
        # mask = self.df_place.identifier.isin(self.df.singleloc.to_list())       # a cartographic ressource represents a single place
        # self.df_place.loc[ mask, "singlecarto" ] = (self.df_place            # for each row of `self.df_place`, the `singlecarto` is the uuid of the first row of `self.df` that represents that place and that place only
        #                                                    .loc[ mask ]
        #                                                    .apply(lambda x: self.df.loc[ x.identifier == self.df.singleloc ].iloc[0].name
        #                                                                     if not pd.isna(x.identifier)
        #                                                                     else np.nan
        #                                                          , axis=1)
        # )
        # self.df_place.singlecarto = self.df_place.singlecarto.replace("", np.nan)
        # # print(self.df_place.loc[ mask, "singlecarto" ].shape[0])                # number of items in `df_place` with a single cartographic ressource representing them
        # # print(len(self.df.loc[ self.df.singleloc.notna(), "singleloc" ].unique())) # number of unique places in `self.df_place` represented by only one row of `self.df` (a single place can be represented by several rows in `self.df` with `singlecarto` not nan)
        # print(f"* {self.df_place[ self.df_place.singlecarto.notna() ].shape[0]} out of {self.df_place.shape[0]} "
        #       + "entries in `self.df_place` have one or more shapefile(s) and image describing this place only")

        # # 2) count number of entries in `df_place` described by one or several
        # #    rows of `df`. for those entries, we'll have a shapefile and geojson
        # #    describing the place and possibly several other places
        # self.df_place["hasloc"] = (self.df_place
        #                                   .identifier
        #                                   .apply(lambda x: any( x in _ for _ in self.df.id_place.to_list()) )
        # )
        # print(f"* {self.df_place.loc[ self.df_place.hasloc ].shape[0]} out of {self.df_place.shape[0]} "
        #       + "rows in `self.df_place` are described by at least one entry in the cartography df")

        # # 3) count number of entries in `self.df_place` described by no element of `self.df`
        # self.df_place["noloc"] = (self.df_place
        #                                  .identifier
        #                                  .apply(lambda x: not any( x in _ for _ in self.df.id_place.to_list()) )
        # )
        # print(f"* {self.df_place.loc[ self.df_place.noloc ].shape[0]} out of {self.df_place.shape[0]} "
        #       + "rows in `self.df_place` are described by no entry in the cartography df")

    #def _place2df(self, df_carto:DFType) -> DFType:
    #    """
    #    transfer data and make joins from `df_carto` to `self.df_place`
    #    1) replace the pointer from `self.df` to `self.df_place`
    #       from a human-readable id to an uuid (the index of `self.df_place`)
    #    2) if a row in `df_carto` points to a single entry in `self.df_place`, then
    #       move `df_carto.vector` to `self.df_place.vector`. `df_carto.vector` stays untouched
    #    3) in the rows of `df_carto.place` where we have extracted a vector, calculate a
    #       centroïd and add it to `self.df_place.latlng`
    #    """
    #    # step 1)
    #    df_carto["countloc"] = df_carto.apply(lambda x: len(x.id_place), axis=1)
    #    print(f"* {df_carto.countloc.sum()} place items before their conversion into uuids in `df_carto`")
    #    df_carto.id_place = df_carto.id_place.apply(lambda x: [ self.df_place
    #                                                          .loc[ self.df_place.identifier == _ ]
    #                                                          .squeeze()
    #                                                          .name
    #                                                      for _ in x ])
    #    print(f"* {df_carto.countloc.sum()} place items after their conversion into uuids in `df_carto`")
    #    # step 2)
    #    sc = self.df_place.loc[ self.df_place.singlecarto.notna(), "singlecarto" ]  # series mapping uuids from `self.df_place` to uuids of `self.df` from which to fetch a vector
    #    df_vector = df_carto.loc[ sc.to_list(), "vector" ].to_frame(name="vector")              # df mapping uuids of `self.df` to their vector representation
    #    self.df_place = (self.df_place
    #                            .reset_index()
    #                            .merge(df_vector, how="left", left_on="singlecarto", right_on="index")
    #                            .set_index("index")
    #                            .rename(columns={"vector_y": "vector"})
    #    )
    #    # verify the validity of the join
    #    def compare(idx:str, gj1:t.Dict, gj2:t.Dict) -> None:
    #        """
    #        verify that the proper geojson was inserted from `df_carto` to `df_place`.
    #        we only verify the `name`, which is an unique identifier for our geojsons
    #        :param idx: the index of the row of `self.df_place`
    #        :param gj1: the geojson from `df_carto`
    #        :param gj2: the geojson from `df_place`
    #        :raises: ValueError if gj1.name != gj2.name
    #        """
    #        if not gj1["name"] == gj2["name"]:
    #            raise ValueError("invalid GeoJSON was inserted in `df_place` on row `{ idx }`"
    #                             + f" from `df_carto`: \n\n"
    #                             + f"* `df_carto.vector`            : { gj1 }\n\n"
    #                             + f"* `df_place.vector`   : { gj2 }\n\n")
    #        return
    #    (self.df_place.loc[ self.df_place.singlecarto.notna() ]
    #                     .apply(lambda x: compare(x.name
    #                                              , df_carto.loc[x.singlecarto, "vector"]
    #                                              , x.vector)
    #                            , axis=1)
    #    )
    #    # step 3)
    #    def geojson2centroid(gj:t.Dict) -> t.List[float]:
    #        """
    #        convert a geojson to a centroid of type [<latitude:float>, <longitude:float>]
    #        epsg:4326 is a geographic (3D, in lat/long) CRS => we need to convert
    #        the DF to `epsg:3857` (its projected, plane equivalent) to get the
    #        centroid before converting back to epsg:4326
    #        :param gj: the geojson
    #        :returns: the centroid
    #        """
    #        epsg = gj['crs']['properties']['epsg_code']
    #        gdf =gpd.GeoDataFrame.from_features(gj["features"], crs=f"epsg:{epsg}")
    #        centroid = gdf.dissolve().to_crs(epsg=3857).centroid.to_crs(epsg=epsg)  # `to_crs()`: see docstring for an explanation
    #        return [ centroid.x.squeeze(), centroid.y.squeeze() ]                   # convert to an array of `[ <lat>, <long> ]` floats
    #    self.df_place["centroid"] = None
    #    self.df_place.loc[ self.df_place.vector.notna(), "centroid" ] = (self.df_place.loc[ self.df_place.vector.notna(), "vector" ]
    #                                                                                           .apply(geojson2centroid))
    #    # replace `nan` values in `df_place.vector` and
    #    # `df_place.centroid` by `{}` and `[]`, respectively
    #    self.df_place.loc[ self.df_place.vector.isna(), "vector" ] = [ {} for _ in range(self.df_place.vector.isna().sum()) ]
    #    self.df_place.centroid = self.df_place.centroid.fillna("[]").astype(str).apply(ast.literal_eval)
    #    return df_carto








