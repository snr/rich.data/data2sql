from typing import List

from .strings import clustering


# *********************************************************
# currently unused !
# multiprocess related operations
# *********************************************************


def initpool(obj) -> None:
    """
    initialize a multiprocessing pool by creating a shared
    memory object (that is, an object whose memory is shared
    between processes: it can be accessed and modified by all 
    processes at the same time.
    
    basically this function just makes `obj` a global variable
    and must be called as the `initializer` arg of `Pool`:
    if you want `xyz` to be a shared memory object, just do:
    ```
    xyz = "hello"
    p = Pool( 10, initializer=initpool, initargs=(xyz,) )
    ```
    :param obj: the object to share between processes. 
    """
    global sm  # sm = shared memory
    sm = obj
    return


def poolworker(argarr:List[str|List[str]]) -> None:
    """
    worker process to manage the grouping of `streetsimp`
    (a group of strings, second element of `argarr` by
    similarity.
    
    must be called in a `multiprocessing.Pool().map()`, inside
    a `Pool` initialized with `initpool`.
    
    :param argarr: a tuple composed of: 
                   * `s`, the clustering key (string to compare against)
                   * `streetsimp`: the strings to be grouped by similarity to `s`
    :returns: none
    """
    s, streetsimp = argarr
    if ( len(sm._getvalue()) == 0 
         or not any(s in cluster for cluster in sm._getvalue()) ):
        sm.append( clustering(s, streetsimp) )
    return sm



