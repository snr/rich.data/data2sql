# from rapidfuzz.distance.Levenshtein import normalized_similarity as lvns
from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from shutil import get_terminal_size
from statistics import mode
import rapidfuzz as rf
import pandas as pd
import numpy as np
import typing as t
import random
import roman
import re

from .strings import isempty, isanon, simplify, date2range, fuzzydate, build_uuid
from .constants import DATASETS


# *****************************************
# globally useful functions
# *****************************************


def unnest(ls_a:t.List, ls_b:t.Optional[t.List]=None) -> t.List:
    """
    unnest the list `ls_a`, using recursion.
    based on: https://stackoverflow.com/a/67135332/17915803

    :param ls_a: the list to unnest
    :param ls_b: an inner list, used when `unnest()` is called recursively
    """
    if ls_b is None:
        ls_b = []
    for x in ls_a:
        if isinstance(x, list):
            unnest(x, ls_b)
        else:
            ls_b.append(x)
    return ls_b


def reorder(df:DFType, k:str, customheader:bool=True) -> DFType:
    """
    reorder columns of `df` according to their order.
    this ensures that we preserve the order of columns when writing to csv.

    if `customheader` is true, they are reordered according to
    the order of `DATASETS[k][0]`. all columns of `df` not in `DATASETS[k][0]`
    are DROPPED.
    else, all columns of `df` are sorted alphabetically and preserved

    this function should
    !!!!!!!!!ALWAYS BE USED WHEN WRITING TO CSV with `df.to_csv()`!!!!!!!!!

    :examples:
    `df.pipe(reorder, k).to_csv(<...>)`
    `reorder(df, k).to_csv(<...>)`

    :param df: the dataframe to reorder
    :param k: a key of `DATASETS`, to read column names
    :returns: the reordered dataframe
    """
    if customheader:
        return df[ DATASETS[k][0][1:] ]
    else:
        return df[ sorted(df.columns) ]


def deduplicate(s:SType, colname:str) -> SType:
    """
    deduplicate a series containing either strings
    or an array of strings. to do so, we create 2 dfs:
    * `df`         : a df with `s` and other representations of it
    * `df_clusters`: a df containing clusters calculated from `s`

    :param s     : the column we're processing
    :param dname : the column's name (`<df name>.<column name>`)
    """
    # 1) CHECK THAT THE COLUMN IS VALID (`Union[str, t.List[str]]`)
    errmsg = (f"functions.deduplicate() on `{colname}`: expected type "
             +f"`str` or `t.List[str]]`, got `{type(s.loc[s.first_valid_index()])}`:\n"
             +f"`{s.loc[s.first_valid_index()]}`")
    # condition 1: contains only lists containing strings
    if isinstance(s.loc[s.first_valid_index()], list):
        assert all( len(_)==0 or isinstance(_[0], str) for _ in s ), errmsg
    # condition 2: contains only strings or nan
    elif isinstance(s.loc[s.first_valid_index()], str):
        assert all( isinstance(_, str) for _ in s ), errmsg

    # 2) BUILD `s_filter` AND `df`.
    # `s_filter`: a series containing only scalars and no
    # np.nan values derived from `s`. we keep `s` intact
    # and only update it at the end
    islist = isinstance(s.loc[s.first_valid_index()], list)  # True: column contains t.List[str]; False: column contains str
    if islist:
        s_filter = s.explode()
        s_filter = s_filter.loc[s_filter.notna()]
    else:
        s_filter = s.loc[s.notna()]

    # `df``: a dataframe mapping values of
    # `s_filter` to simplified and mode value
    df = pd.DataFrame({ "src":s_filter,    # `src`  : source string
                        "simp":np.nan,     # `simp` : same simplified string
                        "_mode": np.nan }) # `_mode`: the correct (clusterized) value to replace `src` by
    df.simp = df.src.apply(simplify).str.replace(" ", "")  # lowercase, normalized accents, no spaces

    # 3) CALCULATE CLUSTERS AND BUILD `clusters`
    choices = df.simp.to_list()  # values to cluster by. `to_list()` gives duplicates, which we need in order to calculate the mode !
    clusters = []                # [ [<cluster 1>], [<cluster 2>], ...]
    processed = []               # [ <list of words that have aldready been clustered> ]. allows to avoid processing the same word twice
    def clustering(val:str):
        """
        we use `DamerauLevenshtein`, (Levenshtein that takes
        into accound permutations, which happen ofter).

        :param val     : the value to cluster
        :param choices : the values against which to cluster
        """
        if val not in processed:
            cutoff = 2  # allow a maximum of 2 changes between `val` and values of `choices`
            cluster = [ c for c in choices
                        if rf.distance.DamerauLevenshtein.distance(val, c) <= cutoff ]
            clusters.append(cluster)
            [ processed.append(c) for c in cluster if c not in processed ]  # all the aldready clusterized values won't be clusterized again
        return val
    df.simp.apply(clustering)

    # 4) MAP `df.src` TO A NON-SIMPLIFIED MODE VALUE
    # df_clusters: a dataframe to perform operations on clusterized vals
    # col1: associated value from cluster (to replace from)
    # col2: simplified mode value (to replace with)
    # col3: full value associated to the mode
    df_clusters = (pd.DataFrame( [ [c, mode(c), np.nan] for c in clusters ]
                               , columns=["cluster", "_mode", "mode_full"])
                     .explode("cluster")
                     .drop_duplicates(subset="cluster")
                     .reset_index(drop=True) )
    fetch = lambda x: df.loc[ df.simp.eq(x), "src" ].iloc[0]  # `df.src` of the first row where `df_clusters._mode` == `df.simp`
    df_clusters.mode_full = df_clusters._mode.apply(fetch)

    # map each non-clusterized in `df` value
    # to its full clusterized counterpart
    fetch = lambda x: (df_clusters.loc[ df_clusters.cluster.eq(x), "mode_full" ]   # df_clusters.mode_full of the first row where `df_clusters.cluster` == `df.simp`
                                  .iloc[0])
    df._mode = df.simp.apply(fetch)

    # check that no `np.nan` values have been inserted
    assert all( df._mode.notna() ), \
           (f"functions.deduplicate(): on `{colname}`, "
           +f"{df._mode.isna().sum()}  empty values inserted "
           + "in `df` after clustering. this may cause data loss")

    # 5) UPDATE `s`
    # create `s_filter` from `df`, unexploding it if necessary
    # (we need to create a new variable to avoid duplicate indexes)
    # then, update `s` with the mode values from `s_filter`
    if islist:
        mklist = lambda x: list(set( _ for _ in x if not pd.isna(_) ))  # deduplicated list without `np.nan` values
        s_filter_out = (df.groupby("index")._mode.apply(mklist) )
    else:
        s_filter_out = df.loc[df._mode.notna(), "_mode"]
    og_shape = s.shape[0]  # number of rows pre-update
    s.update(s_filter_out)

    assert s.shape[0] == og_shape, \
           (f"functions.deduplicate(): on `{colname}`, "
            +f"from {og_shape} to {s.shape[0]} rows"
            + "after running the function")

    return s


def testforeignkey( idx:t.Union[t.List[str], str]
                  , df:DFType
                  , _from:str
                  , _to:str
                  , _fromcol:str ) -> None:
    """
    test that the foreign key(s) in `idx` are indexes
    of `df`. `idx` can be a single foreign key or an array

    :param idx: a foreign key or a list of foreign keys
    :param df: the dataframe that the foreign key(s) `idx`
               points to. `idx` must be an index of `df`
    :param _from: the name of the dataframe where the foreign
                  key(s) `idx` is on
    :param _to: the name of the dataframe where the foreign key on
                `_from` points to (that is, the name of `df`)
    :param _fromcol: the column name in the `_from` df
    """
    if isinstance(idx, list):
        for _ in idx:
            if df.loc[ df.index == _ ].shape[0] == 0:
                raise AssertionError( f"foreign key `{_}` found in `{_from}.{_fromcol}` "
                                     +f"is not an index of `{_to}`")
    else:
        if df.loc[ df.index == idx ].shape[0] == 0:
            raise AssertionError(f"foreign key `{idx}` found in {_from}.{_fromcol}` "
                                +f"is not an index of `{_to}`")
    return


def dummycol(df:DFType, col:str, islist:bool) -> DFType:
    """
    in some cases, columns are empty => build dummy data
    to make joins.

    :param df: the dataframe we're working on
    :param col: the column on which to act
    :param islist: each cell will contain an list of values instead of a single value
    """
    if df[col].astype(str).replace("[]", np.nan).notna().sum() > 0:
        raise ValueError(f"`dummycol()`: `df` aldready contains data in column `{col}`")

    words = ["Claire", "Fontaine", "uses", "the", "concept", "of", "the", "readymade", "as", "a", "way", "of", "criticising", "production", "disguised", "as", "a", "creation", "of", "more", "and", "more", "artefacts", "that", "are", "desirable", "because", "they", "superficially", "appear", "as", "new.", "Generally", "she", "works", "with", "appropriation", "on", "a", "formal", "level", "and", "she", "hijacks", "contents,", "using", "sculpture,", "installation,", "video", "and", "painting", "to", "create", "an", "emotionally", "loaded", "criticism", "of", "the", "author", "and", "the", "forms", "of", "authority", "at", "this", "stage", "of", "capitalism.", "This", "aesthetic", "approach", "that", "she", "describes", "as", "expropriation,", "a", "way", "of", "giving", "an", "existential", "use", "value", "to", "pre-existing", "objects", "and", "artworks,", "also", "addresses", "the", "general", "crisis", "of", "singularity,", "which", "she", "describes", "as", "the", "individual", "and", "collective", "impossibility", "to", "give", "a", "meaning", "to", "one's", "life", "under", "the", "current", "political", "circumstance", "and", "the", "systematic", "surveillance,", "repression", "and", "countless", "limitations", "of", "our", "freedom.", "Claire", "Fontaine", "prefers", "to", "integrate", "the", "existing", "art", "circuit", "to", "create", "complicities", "and", "foster", "change", "which", "entails", "partaking", "in", "the", "mechanisms", "and", "subjects", "of", "the", "art", "industry", "including", "collectors,", "dealers", "and", "institutions.[5]"]
    if islist:
        df[col] = df[col].apply(lambda x: [ words[ random.randint(0,len(words)-1) ]
                                            for _ in range(random.randint(0,9)) ])
    else:
        df[col] = df[col].apply(lambda x: " ".join(words[ random.randint(0,len(words)-1) ]
                                                   for _ in range(random.randint(0,9))))
    return df


def cell2address(cell:str, srccol:str, srcuuid:str) -> t.Dict:
    """
    from an address, create a row of the table `address`.

    :param cell: the string representation of the address
    :param srccol: the column name on which we're working
    :param srcuuid: the uuid of the row we're working on
    :returns: `addr` (see structure below)
    """
    # map column names to the same values that will be present in
    # `df_cartography.vector_source` and `df_place.map_source`
    # they are not supposed to be exposed to the public.
    sources = { "plot1820"  : "feuille",
                "vasserot"  : "vasserot",
                "atlas1860" : "parcellaire1900",
                "current"   : "contemporain" }
    # { <column name>: <date> }
    dates = { "plot1820"  : [1837, 1840],
              "vasserot"  : [1810, 1836],
              "atlas1860" : [1860, 1900],
              "current"   : [1900, 2050]  }

    addr = {
        "address": cell,             # street name and number
        "date"   : dates[srccol],    # date for the address
        "source" : sources[srccol],  # which source the infos come from
        "srcuuid": srcuuid           # uuid of the row it comes from
    }
    # # extract roman numbers
    # roman_num = re.search("^\s*([MCDXLIV]+)(?!\w)", cell)  # roman number
    # arabic_num = re.search("^\s*((\d|et|bis|\s)+)", cell)  # arabic numerals, with "et" and "bis"
    # if roman_num:
    #     addr["number"] = str(roman.fromRoman( roman_num[1] ))
    #     addr["street"] = re.sub(f"{roman_num[0]}\s*,*", "", cell).strip()
    # elif arabic_num:
    #     addr["number"] = arabic_num[1]
    #     addr["street"] = re.sub(f"{arabic_num[0]}\s*,*", "", cell).strip()
    # elif not re.search("[Nn]on\sexistant", cell):  # delete cells where `Non existant` is written
    #     addr["street"] = cell

    return addr


from .io import read_df  # circular imports


# *******************************************************
# UNUSED FUNCTIONS
# *******************************************************

# def get_ro(
#     k: str
#     , verifier: t.Dict[str, str|int]
#     , fuzzy: bool = False
# ) -> SType:
#     """
#     return csv rows in `df` that match the column-value
#     pairs in `verifier` as a pandas dataframe.
#     * if `fuzzy == False`, a perfect match is performed
#     * if `fuzzy == True`, perform a fuzzy match by calculating a
#       levenshtein ratio between all values in `verifier` and the
#       dataframe rows.
#     * if no row is matched, an empty dataframe is returned.
#       it can be verified using **`df.shape[0] == 0`**, which
#       shows there are 0 rows.
#
#     the operation in `fuzzy == False` is adapted from the first
#     response and its comments in this thread:
#     https://stackoverflow.com/questions/34157811/filter-a-pandas-dataframe-using-values-from-a-dict
#
#     :param k: the key of `DATASETS` that points to
#               the filepath and headers
#     :param verifier: a dict of { "column name": "value" }
#     :returns: the matched rows as a dataframe
#     """
#     df = read_df(k)
#
#     # drop columns with empty values from the `verifier` series
#     verifier = (pd.Series(verifier)
#                   .replace("^\s*$", np.nan, regex=True)
#                   .dropna())
#
#     # fuzzy match rows: create a new dataframe containing only
#     # rows from `df` that match `verifier`
#     if fuzzy:
#         # concatenate all matching columns in `verifier`
#         # and `df` to a new column `concat_`. we use `simplify()`
#         # to normalize the strings
#         df["concat_"] = (
#             df.loc[:, verifier.index.values]  # `df.loc[:, colnames]` - the `:` means that we select the whole row. as a reminder, the syntax is: `df.loc[rowindex, colname]`
#               .apply(
#                   lambda x: simplify( "".join(x.values.astype(str)) )
#                   , axis=1
#               )
#         )
#         verifier = pd.DataFrame([verifier])
#         verifier["concat_"] = (
#             verifier[verifier.columns.values]
#                     .apply(
#                         lambda x: simplify( "".join( x.values.astype(str)) )
#                         , axis=1
#                     )
#         )
#         verifier = verifier.squeeze(axis=0)  # convert verifier back to a series
#
#         # calculate the levenshtein ratio between `df.concat_` and `verifier.concat_`
#         df.concat_ = df.apply(
#             lambda x: rf.distance.Levenshtein( x.concat_, verifier.concat_ )
#             , axis=1
#         )
#
#         return (df[ df.concat_ >= 0.9 ]                          # drop all rows where `concat_` < 0.9
#                   .sort_values(by=["concat_"], ascending=False)  # order by `concat_`, highest lev ratio first
#                   .head(1)                                       # keep only the 1st row
#                   .drop("concat_", axis=1)                       # delete `concat_` col
#         )
#
#     # exact match rows and return a dataframe
#     else:
#         return df.loc[(
#             df[verifier.index.values] == verifier
#         ).all(axis=1)]
#
#     return

# def deduplicate( df:DFType
#                , name:str
#                , cols:t.List[str]
#                # , differenciate:t.List[str]=[]
#                ) -> DFType:
#     """
#     __THIS FUNCTION IS NON-DETERMINISTIC AND CAN CAUSE UNEXCEPTED BEHAVIOUR__
#
#     deduplicate `df` using the columns in `cols`:
#     if several rows in `df` have similar values for `cols`,
#     then the duplicate rows are dropped.
#
#     basically we group values in `cols` in clusters, select one
#     value to keep in each cluster, drop other rows and create
#     a mapper of `{ <dropped uuid>: <kept uuid> }`
#
#     must be used before `updateforeignkey()` and `drop_deduplicated()`.
#
#     :param df            : the dataframe
#     :param name          : the name of the dataframe (`df_theme`...)
#     :param cols          : list of column names on which to deduplicate
#     :param differenciate : list of columns names. if in two rows that should be
#                            deduplicated, the values in `differenciate` are
#                            different, don't deduplicate
#     """
#     simpmap  = {}  # { <uuid>: <simplified concatenation of `cols`> }
#     clusters = []  # variable to store clusterized values. its structure changes a lot
#     repl     = {}  # dict that replaces keys in `clusters` by their corresponding uuid: { <key (simplified name)>: <selected uuid> }
#     mapper   = {}  # our final dict: map the uuids for the rows that will be dropped to the uuid from their mode { <dropped uuid>:<mode uuid> }
#
#     # create `simpmap`
#     print(df[cols])
#     df["concat_cols"] = df[cols].apply(lambda x: "".join("".join(filter(str.isalnum, _)).lower()
#                                                          for _ in x.to_list()), axis=1)
#     print(df.concat_cols)
#     print(df.concat_cols.isna().sum())
#     simpmap = df.concat_cols.to_dict()
#
#     # clusterize
#     # `cluster` is [ [<array of similar simplified vals>], [<array of similar simplified vals] ]
#     for v in simpmap.values():
#         clusters.append([
#             r[0] for r in rf.process.extract( v
#                                             , simpmap.values()
#                                             , processor=None
#                                             , score_cutoff=89)
#         ])
#
#     # update cluster to have the structure: { <mode value>: [<array of other values>] }
#     # (mode = the value we keep from the cluster)
#     clusters = { mode(c):c for c in clusters if len(c) > 0 }
#     # print(clusters)
#     # print(simpmap)
#     print(">", len(list(clusters.items())))
#
#     # update cluster to have the structure: { <mode uuid>: [<other uuids>] }
#     for k_clusters,v_clusters in clusters.items():
#         # print(k_clusters, v_clusters)
#         clusters[k_clusters] = [ k for k,v in simpmap.items() if v in v_clusters ]
#         # print(clusters[k_clusters], len(clusters[k_clusters]),  "**")
#
#         # transform `k_clusters` into an uuid
#         # if there's only one entry in `v_clusters`, it's easy: no replacement is done
#         if len(clusters[k_clusters]) == 1:
#             repl[k_clusters] = clusters[k_clusters][0]
#         # else, we need to determine uuid of the row to keep in `v_clusters`
#         else:
#             df_cluster = df.loc[ df.concat_cols == k_clusters ]
#             df_cluster = df_cluster.astype(str).drop_duplicates()  # remove duplicates
#             # if all rows matched by `cluster` are duplicates, keep the uuid of a single row
#             if df_cluster.shape[0] == 1:
#                 repl[k_clusters] = df_cluster.iloc[0].name
#             # else, determine which row to keep. we consider that the
#             # row with the most data is the best one => keep that one
#             else:
#                 df_cluster["collated_row"] = (df[df.columns]
#                                                  .apply(lambda x: "".join( str(_) for _ in x.to_list() )  # collate the string representation of all columns of a row into a cell
#                                                         , axis=1)
#                                                  .apply(len)  # get the length
#                 )
#                 repl[k_clusters] = df_cluster.collated_row.idxmax()  # select the uuid of the row where `collated_cols` is the longest
#
#     # clusters is now `{ <mode uuid>: [<array of other uuids of the cluster>] }`
#     clusters = { repl[k]:v for k,v in clusters.items() }
#     print(">", len(list(clusters.items())))
#
#
#     # update `df`: remove the arrays created here + create a
#     # `pointer` column to allow the updating of foreign keys:
#     # before dropping duplicates to keep only modes in `df`,
#     # the foreign keys pointing to rows in `df` must be updated
#     # to the mode uuid
#     mapper = { _:k for k,v in clusters.items() for _ in v }  # { <non-mode uuid>: <mode uuid> }
#
#     # ALORS LÀ IL Y A UN TRUC QUI NE MARCHE PAS SMH ?
#     # hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
#
#     # df["pointer"] = df.index.to_series().apply(lambda x:mapper[x])
#     df["pointer"] = df.index.to_series().apply(lambda x:x in mapper.keys())
#     print(df.pointer.eq(1).sum(), df.shape[0])
#
#     print(df.loc[df.pointer.eq(0)])
#
#     exit()
#
#     df = df.drop(columns="concat_cols")
#     print("* `deduplicate()`: reduced number of rows from"
#           , f"{len(df.index.to_series().unique())} to {len(df.pointer.unique())} on `{name}`")
#     return df
#
#
# def updateforeignkey(idx:str|t.List[str], df:DFType) -> str|t.List[str]:
#     """
#     replace the foreign keys (or list of foreign keys) pointing to `df`
#     by new foreign key(s), after deduplication of `df`.
#     `idx` can be a single foreign key or an array of foreign keys.
#
#     must be used after `deduplicate()` and before `drop_deduplicated()`.
#
#     :param idx: the foreign key or array of foreign keys to update
#     :param df: the daframe to which the foreign keys point
#     :returns: the updated foreign key or array of foreign keys
#     """
#     if isinstance(idx, list):
#         return [ df.loc[ df.index == _ ].pointer.squeeze()
#                  for _ in idx ]
#     else:
#         return df.loc[ df.index == idx ].pointer.squeeze()
#
#
# def dropdeduplicated(df:DFType) -> DFType:
#     """
#     drop rows that were deduplicated using the `deduplicate()` above
#
#     must be used after `deduplicated()` and `updateforeignkey()`
#
#     :param df: the dataframe we're deduplicating
#     """
#     df["_index"] = df.index.to_series()
#     return df.loc[ df._index == df.pointer ].drop(columns=["_index", "pointer"])


# def cell2actor(cell: str) -> t.Dict:
#     """
#     process a dataset cell in order to extract structured
#     data about an actor. this function should be revised
#     and completed to fit patterns specific to new datasets
#
#     :param cell: the input cell, a string from a pandas series
#     :returns: the `actor` dict below with relevant info
#     """
#     actor = {
#         "last_name": "",  # last name
#         "first_name": "",  # first name
#         "date": [],   # normalized dates
#         "source": ""  # original description
#     }
#     actor["source"] = str(cell).strip()
#     if not isempty(cell) and not re.search("^[\s_\-':;,.!?&/\\\"\[\]\|\(\)]*$", cell):
#         # **base regex**: full names, possibly comma separated,
#         # including composed names (with `.` `-` `&` as separators)
#         # remove `Attribué à` before processing the name
#         # as it can cause mismatches
#         name_match = re.search(
#             "^\s*(([A-ZÀ-Ý]?[a-zà-ÿ]*)[-,'\.\s\&]*)+"
#             , re.sub("^\s*[aA]ttribu[ée]\s+à", "", cell)
#         )
#
#         # check if `cell` contains a value equivalent to `Anonyme`
#         if isanon(cell):
#             actor["last_name"] = "Anonyme"
#
#         elif name_match:
#             # remove trailing `,` + functions/descriptions
#             # (all lowercase words after a comma)
#             name = re.sub(
#                 ",+\s*[a-zà-ÿ-,'\.\s\&]*$"
#                 , ""
#                 , name_match[0].strip()
#             ).strip()
#
#             # if there's a comma in the name, it's easy:
#             # the name follows the pattern `Last name, First name(s)`
#             if "," in name:
#                 actor["last_name"] = name.split(",")[0].strip()
#                 actor["first_name"] = name.split(",")[1].strip()
#                 # print(name, "|", actor["last_name"], "|", actor["first_name"])
#
#             # else, there's a bit more work
#             else:
#                 # in some cases, `.` is used as a comma separator
#                 # between the name and the function => keep only the name
#                 name = re.split("(?<=[a-zà-ÿ]{3})\.", name)[0]
#
#                 # distinguish organisation names and person names.
#                 # to do that,
#                 # - search for long lowercase words -> only in org names
#                 # - search for specific terms that can't be present in person's names
#                 if len(name.split()) > 1:
#                     # organisation name
#                     if (
#                         re.search("((?<=^)|(?<=\s))[a-zà-ÿ]{4,}((?=$)|(?=\s))", name)
#                         or any(
#                             re.search(f"((?<=^)|(?<=\s)){ x.lower() }((?=$)|(?=\s))" , name.lower())
#                             for x
#                             in [ "et", "&", "and", "Agence", "Union"
#                                  , "Charivari", "Galerie", "Commission"
#                                  , "Caricature", "Imprimerie", "Bibliothèque"
#                                  , "Caisse", ]
#                         )
#                     ):
#                         actor["last_name"] = name.strip()
#                     # person name
#                     else:
#                         actor["first_name"] = " ".join( n for n in name.split()[0:-1] ).strip()
#                         actor["last_name"] = name.split()[-1].strip()
#
#                 # if there's only 1 name, no need to distinguish between first/last name
#                 else:
#                     actor["last_name"] = name
#
#         actor["date"] = fuzzydate(cell)
#         actor["date"] = date2range([ d for d in actor["date"] ])
#
#         # check the validity of the results
#         # print(cell)
#         # for k, v in actor.items():
#         #     print(k, "".join( "." for i in range(0, 10-len(k)) ), ":", v)
#         # print("\n\n")
#
#     # empty cell
#     else:
#         actor["last_name"] = "Anonyme"
#
#     return actor
