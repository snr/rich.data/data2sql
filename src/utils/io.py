from threading import local
from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from typing import List, Dict
import pandas as pd
import sys
import os

from .constants import DATASETS, TMP, OUT


# ********************************************
# filesystem operations: read and write to
# files and folders (io = input/output)
# ********************************************


def read_df(
    k:str
    , useindexcol:bool|int
    , customheader:bool
) -> DFType:
    """
    read a csv file as a dataframe.

    about `useindexcol`
    * if `useindexcol==False`, no index col will be used
      and a rangeindex will be created by pandas
    * if useindexcol is an int, then the Nth column will be
      used as the index column (0=1st column...)
    * `useindexcol` cannot be `True`, it is either `False` or `int`
    * `False` must be passed explicitly, not as `0`

    about `customheader`:
    * customheader indicates to use custom headers (see `H_*`
      variables in `utils.constants`,
    * it is useful for renaming columns, but becomes hard to
      manage when renaming columns and changing dataframe
      structure from one writing to another.
    * `customheader` should be used on initial read, but not
      when reading a df written with `write_df(<...>, customheader=True)`

    about `useindexcol`: the index column to use in the table.
    * `False` for no index
    * `int` to select a column by its number

    :param infile: the path to the input file
    :param column_names: custom names for our headers
    :param useindexcol: use an index column from the dataframe.
    :param customheader: use custom header names from `DATASETS`
    :return: df, an initialized dataframe
    """
    if not ( isinstance(useindexcol, int)
             or isinstance(useindexcol, bool)
    ):
        raise TypeError(f"`useindexcol` should be of type `bool` or `int`, got `{useindexcol}`")
    elif ( (isinstance(useindexcol, bool) and useindexcol != False )
           or (isinstance(useindexcol, int) and useindexcol < 0)
    ):
        raise ValueError(f"`useindexcol` must be `False` or a null or positive integer, got `{useindexcol}`")

    return pd.read_csv(
        DATASETS[k][1]
        , sep="\t"
        , quotechar='"'
        , skip_blank_lines=True
        , header=0                                        # if `names=None`, then the 0th row is used as header. else, `customheader` is used
        , names=DATASETS[k][0] if customheader else None  # `None` => 0th line will be used as headers
        , index_col=useindexcol
    )


def write_df(
    k:str
    , df:DFType
    , customheader:bool
    , append:bool
) -> None:
    """
    write an entire pandas dataframe to a csv file. contrary to
    `write_ro`, this wipes the entire csv and rewrites it anew,
    unless `append==True`.

    relevant data is targeted using `k`, a key of `DATASETS` which
    contain the dataframe's headers and the path to the file.

    use of custom headers defined in `DATASETS` can be disabled with
    `customheader=False`, which is better for intermediary steps where
    the dataset structure changes often.

    :param k: the key of `DATASETS` that points to
              the filepath and headers to read
    :param df: the dataframe to write
    :param customheader: use a custom header (DATASETS[k][0]) when
                         writing the df. useful for clarity, but it
                         causes to miss out on some columns in
                         intermediary steps
    :param append: append to an existing csv instead of writing
    """
    # to check that columns aren't messed up:
    # print(len(df.columns.to_list()), df.columns.to_list())

    (df.pipe(reorder, k=k, customheader=customheader)
       .to_csv(
           DATASETS[k][1]
           , sep="\t", quotechar='"', encoding="utf-8"
           , header=False if append else DATASETS[k][0][1:] if customheader else True
           , index_label=None if append else "index"
           , columns=DATASETS[k][0][1:] if customheader else None  # only write columns listed in `DATASETS`
           , mode="a" if append else "w"                           # append/write
        )
    )
    return


def write_ro(k: str, ro:DFType) -> str:
    """
    append `ro`, a single csv row represented as a pd.Series,
    to a file targeted using `k`. if the file doesn't yet exist,
    append the header too.

    * for a much more sophisticated equivalent,
      see the commented `write_ro()` below

    :param k: the key of `DATASETS` that points to
              the filepath and headers
    :param ro: the row to append
    :returns: the UUID of the inserted row
    """
    uuid = ro.name  # output: the uuid of the written row or of the row aldready exists

    # write headers. this is the only use we have for it i think
    if not os.path.isfile(DATASETS[k][1]):
        pd.DataFrame([ro]).to_csv(DATASETS[k][1], sep="\t"
                                  , quotechar='"',  encoding="utf-8"
                                  , header=False, mode="w")

    # write the row. unused i think
    else:
        (pd.DataFrame([ro])
           .pipe(reorder, k)
           .to_csv(DATASETS[k][1], sep="\t", quotechar='"'
                   , encoding="utf-8", header=None
                   , index_label="index", mode="a"))
    return uuid


def write_credfile(db:str) -> None:
    """
    create a file `TMP/credfile.txt` with the name of the
    `postgresql_credentials.json` file to use when creating
    the engine. because `engine` must be available in the
    global context, it can't be called simply using a function,
    and we can't pass the filename as an argument from
    `main` to `engine.py`.

    :param db: `local` or `server`, the `-d --database`
               argument in the CLI
    """
    with open(os.path.join(TMP, "credfile.txt"), mode="w") as fh:
        fh.write("postgresql_credentials_local.json" if db=="local"
                 else "postgresql_credentials_remote.json")
    return


def read_credfile() -> str:
    """
    read the content of `TMP/credfile.txt` to return the name
    of the credentials file to use when connecting to the db
    """
    with open(os.path.join(TMP, "credfile.txt"), mode="r") as fh:
        return fh.read()


def makeout() -> None:
    """
    create `OUT`
    """
    os.makedirs(OUT, exist_ok=True)


def maketmp() -> None:
    """
    * delete the old TMP
    * create the new TMP and its child folders
    """
    os.makedirs(TMP)
    os.makedirs(os.path.join(TMP, "imagefiles"))
    os.makedirs(os.path.join(TMP, "imagefiles", "iconography"))
    os.makedirs(os.path.join(TMP, "imagefiles", "cartography"))
    return


def deltmp(_dir:str=TMP) -> None:
    """
    delete all files/directories in the temp
    directory + the dir itself, recursively.

    :param _dir: the directory to delete. this allows to use the function recursively
    """
    try:
        # check that the directory is in `TMP` to avoid deleting everything
        if not ( os.path.commonpath([ os.path.abspath(TMP) ])
                 == os.path.commonpath([ os.path.abspath(TMP), os.path.abspath(_dir) ])
        ):
            print(f"directory `{_dir}` not in TMP. not deleting the files and exiting")
            sys.exit(1)

        # delete recursively
        for root, dirs, files in os.walk(_dir):
            [ os.remove(os.path.join(root, f)) for f in files ]
            [ deltmp(os.path.join(root, d)) for d in dirs ]

        # once you've deleted all its contents, delete itself `_dir`
        os.rmdir(_dir)

    # pass if `_dir` is aldready deleted
    except FileNotFoundError:
        pass
    return


from .functions import reorder  # avoid circular imports


# *******************************************************
# UNUSED FUNCTIONS
# *******************************************************

# def write_ro(
#     k: str
#     , ro:DFType
#     , verifier:Dict[str, str|int] = {}
#     , append:List[str] = []
#     , fuzzy:bool = False
# ) -> str:
#     """
#     append `ro`, a single csv row represented as a pd.Series,
#     to `outfile`.
#     * if the file doesn't yet exist, append the header too.
#     * if the file exists, check if the row we're appending
#       aldready exists in the dataframe by filtering using
#       `verifier` (see `get_ro`).
#       * if `append` is not empty, then values for column names
#         in `append` will be updated if the new values are longer
#         than the ones aldready written in `outfile`
#
#     :param k: the key of `DATASETS` that points to
#               the filepath and headers
#     :param ro: the row to append
#     :param verifier: a dict of { "column name": "value" }
#     :param append: a list of column names to update if the new
#                    values are longer than the old ones
#     :param fuzzy: check that `verifier` matches fuzzily instead
#                   of doing an exact match (see `get_ro`)
#     """
#     filtermatch = 0     # number of matched rows by `get_ro()`
#     uuid = ro.name  # output: the uuid of the written row or of the row aldready exists
#
#     # write headers at first
#     if not os.path.isfile(DATASETS[k][1]):
#         pd.DataFrame( [ro] ).to_csv(
#             DATASETS[k][1], sep="\t", quotechar='"'
#             , encoding="utf-8", header=False, mode="w"
#         )
#
#     # append a new rows to an existing dataset. in that case,
#     # filter `ro` by `verifier` to avoid adding duplicate rows.
#     # update the rows if necessary. we only update the 1st row, as
#     # we assume that there can be no duplicate rows in the output file
#     else:
#         updated = False  # logger to indicate that an existing row has been updated. if True, the entire csv file is rewritten
#         unique = True    # logger to indicate that `ro` is not present in the output csv (yes, we need that too). if True, a new dataset ro is appended
#         if len(verifier.keys()) > 0:
#             df_filter = get_ro(k, verifier, fuzzy).fillna("")
#             filtermatch = df_filter.shape[0]
#             uuid = df_filter.iloc[0].name if filtermatch > 0 else uuid  # get the id of the matched row if a row has been matched
#
#             # now this is would be weird: our filter doesn't work
#             if filtermatch > 1:
#                 print("ERROR ON FILTER IN `write_ro()`:\n"
#                       + f"{filtermatch} matched rows for "
#                       + f"`{verifier}` on table `{k}` \n\n")
#                 sys.exit(1)
#
#             # not unique, but no need to update
#             elif filtermatch == 1 and len(append) == 0:
#                 unique = False
#
#             # the column may be updated if necessary
#             elif filtermatch == 1 and len(append) > 0:
#                 unique = False                 # a matching row has been found
#                 outfile_s = df_filter.iloc[0]  # series that match `df_filter` in the output csv: the row to update
#                 for colname in append:
#                     if len(ro[colname]) > len(outfile_s[colname]):
#                         outfile_s[colname] = ro[colname]
#                         updated = True
#
#                 # write the whole dataframe with the updated row
#                 # if updated == False, nothing is written to the df
#                 # since no update is necessary for the row
#                 if updated == True:
#                     df = read_df(k)
#                     df.loc[outfile_s.name] = outfile_s
#                     write_df(k, df)
#
#         # `ro` isn't present in the output csv or we
#         # haven't checked for duplicates => write the row
#         if unique == True:
#             (pd.DataFrame([ro])
#                .pipe(reorder, k)
#                .to_csv(
#                    DATASETS[k][1], sep="\t", quotechar='"'
#                    , encoding="utf-8", header=None
#                    , index_label="index", mode="a"
#                )
#             )
#     return uuid

# def deltmp():
#     """
#     delete the `TMP` folder
#     """
#     try:
#         for f in os.listdir(TMP):
#             os.remove(os.path.join(TMP, f))
#         os.rmdir(TMP)
#     except FileNotFoundError:
#         pass
#     return


