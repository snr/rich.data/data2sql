import os
from telnetlib import OUTMRK


# file paths
UTILS = os.path.abspath(os.path.dirname(__file__))
SRC = os.path.abspath(os.path.join(UTILS, os.pardir))
ROOT = os.path.abspath(os.path.join(SRC, os.pardir))
IN = os.path.abspath(os.path.join(ROOT, "in"))
OUT = os.path.abspath(os.path.join(ROOT, "out"))
IN_PLACE = os.path.abspath(os.path.join(IN, "place"))
IN_DIRECTORY = os.path.abspath(os.path.join(IN, "directory"))
IN_CARTOGRAPHY = os.path.abspath(os.path.join(IN, "cartography"))
IN_ICONOGRAPHY = os.path.abspath(os.path.join(IN, "iconography"))
CONFIDENTIALS = os.path.abspath(os.path.join(UTILS, "confidentials"))
TMP = os.path.abspath(os.path.join(SRC, "tmp"))

# prepared tables (or, tables that won't be extracted from the
# csv datasets). they are all represented as lists of lists.
# the uuid can be added later, during sql insertion
INSTITUTIONS = [
    # paris musées
    [ "PM"     , "Paris Musées" ],
    [ "MC"     , "Musée Carnavalet" ],
    [ "PG"     , "Palais Galliera" ],
    [ "MMB"    , "Maison de Balzac" ],
    [ "MVH"    , "Maison Victor Hugo" ],
    [ "MVR"    , "Musée de la Vie Romantique" ],
    [ "PP"     , "Petit Palais. Musée des Beaux-Arts de la Ville de Paris" ],
    [ "MB"     , "Musée Bourdelle" ],
    [ "ML"     , "Musée de la Libération Leclerc Moulin" ],

    # bibliothèques spécialisées de la ville de Paris
    [ "BSVP"   , "Bibliothèques spécialisées de la Ville de Paris" ],
    [ "BHVP"   , "Bibliothèque historique de la Ville de Paris" ],
    [ "BHD"    , "Bibliothèque de l'Hôtel de Ville" ],
    [ "BD"     , "Bibliothèque Marguerite Durand" ],
    [ "F"      , "Bibliothèque Forney" ],
    [ "MMP"    , "Médiathèque musicale de Paris" ],
    [ "BILIPO" , "Bibliothèque des littératures policières" ],
    [ "BTV"    , "Bibliothèque du tourisme et des voyages - Germaine Tillion" ],

    # autres musées et institutions importantes
    [ "BNF"    , "Bibliothèque nationale de France" ],
    [ "BM"     , "British Museum" ],
    [ "CVP"    , "Comission du vieux Paris" ],
    [ "INHA"   , "Institut national d'histoire de l'art" ],
    [ "MAK"    , "Musée Albert Kahn" ],

    # archives
    [ "AN"     , "Archives nationales" ],
    [ "AP"     , "Archives de Paris" ],

    # autres bibliothèques municipales
    [ "MD"     , "Médiathèque Marguerite Duras" ],
    [ "BMR"    , "Bibliothèque municipale et partimoniale Villon" ]
 ]


ADMIN_PERSON = [
    [ "Charlotte", "Duvette", "0009-0000-1109-678X"  ]
    , [ "Louise", "Thiroux", ""  ]
    , [ "Teoman", "Akgonul", ""  ]
    , [ "Justine", "Gain", ""  ]
    , [ "Esther", "Dasilva", ""  ]
    , [ "Louise", "Baranger", ""  ]
    , [ "Loïc", "Jeanson", "0000-0001-7452-0648"  ]
    , [ "Paul", "Kervegan", "0000-0003-2821-8821"  ]
    , [ "Colin", "Prudhomme", ""  ]

    , [ "Isabella", "Di Leonardo", "0000-0002-1747-9164"  ]
    , [ "Raphaël", "Barman", "0000-0002-0834-177X"  ]
    , [ "Albane", "Descombes", "0000-0003-2414-4589"  ]
    , [ "Marion", "Kramer", ""  ]
    , [ "Ravinitesh", "Annapureddy", "0000-0003-4383-020X"  ]
 ]

LICENSE = [
    [ "CC BY 4.0", "https://creativecommons.org/licences/by/4.0/"  ]
    , [ "Other", "Licence non encore définie"  ]
 ]

# headers for our dataframes
# * `H_IN` matches input column names;
# * others match the SQL tables and the dataframes at the
#   end of the joining process. it will be verified that
#   the columns of all our dataframes are the same as the
#   `H_*` columns !
H_IN_ICONOGRAPHY = [
    "index"
    , "author_1"
    , "author_2"
    , "commentary"
    , "corpus"
    , "date"
    , "date_corr"
    , "date_source"
    , "description"
    , "id_place"
    , "id_richelieu"
    , "iiif_folio"
    , "iiif_url"
    , "inscription"
    , "institution"
    , "inventory_number"
    , "named_entity"
    , "produced_richelieu"
    , "publisher"
    , "represents_richelieu"
    , "source_url"
    , "technique"
    , "theme"
    , "title"
    , "title_secondary"
    , "filenames"

    # "index"
	# , "author_1"
	# , "author_2"
	# , "commentary"
	# , "corpus"
	# , "date"
	# , "date_corr"
	# , "date_source"
	# , "description"
	# , "id_og"
	# , "id_place"
	# , "iiif_folio"
	# , "iiif_url"
	# , "inscription"
	# , "institution"
	# , "inventory_number"
	# , "named_entity"
	# , "produced_richelieu"
	# , "publisher"
	# , "represents_richelieu"
	# , "source_url"
	# , "technique"
	# , "theme"
	# , "title"
	# , "title_secondary"
	# , "filenames"
 ]

H_IN_PLACE = [
    "index"
    , "current"
    , "atlas1860"
    , "plot1820"
    , "vasserot"
    , "id_richelieu"
 ]
H_IN_DIRECTORY = [
    "gallica_ark"
    , "gallica_row"
    , "entry_name"
    , "occupation"
    , "street"
    , "number"
    , "date"
    , "gallica_page"
    , "tags"

 ]
H_IN_CARTOGRAPHY = [
    "index"
    , "id_place"
    , "title"
    , "date_source"
    , "inventory_number"
    , "source_url"
    , "credits"
    , "map_source"
    , "granularity"
    , "raster"
    , "institution"
    , "latlngbounds"
    , "vector"
    , "date"
    , "crs_wkt"
    , "crs_epsg"
 ]
#H_IN_ACTOR = [
#    "index"
#    , "src"
#    , "first_name"
#    , "last_name"
#    , "date"
# ]
H_ICONOGRAPHY = [
    "index"
    , "id_uuid"
    , "id_richelieu"
    , "corpus"
    , "date"
    , "date_source"
    , "date_corr"
    , "description"
    , "inscription"
    , "inventory_number"
    , "produced"
    , "represents"
    , "technique"
    , "iiif_url"
    , "iiif_folio"
    , "source_url"
    , "id_licence"
 ]
H_CARTOGRAPHY = [
    "index"
    , "id_uuid"
    , "source_url"
    , "date_source"
    , "date"
    , "inventory_number"
    , "id_licence"
    , "title"
    , "vector"
    , "granularity"
    , "map_source"
    , "crs_epsg"
 ]
H_DIRECTORY = [
    "index"
    , "id_uuid"
    , "gallica_ark"
    , "gallica_row"
    , "gallica_page"
    , "entry_name"
    , "occupation"
    , "date"
    , "tags"
    , "id_address"
    , "id_licence"
 ]

H_FILENAME = [
    "index"
    , "id_uuid"
    , "url"
    , "latlngbounds"
    , "id_iconography"
    , "id_cartography"
    , "id_licence"
 ]
H_PLACE = [
    "index"
    , "id_uuid"
    , "centroid"
    , "vector"
    , "date"
    , "id_place_group"
    , "vector_source"
    , "id_richelieu"
    , "crs_epsg"
 ]
H_ADDRESS = [
    "index"
    , "id_uuid"
    , "address"
    , "city"
    , "country"
    , "date"
    , "source"
 ]
H_INSTITUTION = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "description"
 ]
H_ACTOR = [
    "index"
    , "id_uuid"
    , "entry_name"
 ]
H_NAMED_ENTITY = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "description"
 ]
H_THEME = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "description"
 ]
H_ANNOTATION = [
    "index"
    , "id_uuid"
    , "content"
    , "id_iconography"
 ]
H_TITLE = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "ismain"
    , "id_iconography"
 ]
H_PLACE_GROUP = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "description"
 ]
H_R_ICONOGRAPHY_ACTOR = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_actor"
    , "role"
    , "ismain"
 ]
H_R_ICONOGRAPHY_NAMED_ENTITY = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_named_entity"
 ]
H_R_ICONOGRAPHY_THEME = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_theme"
 ]
H_R_ICONOGRAPHY_PLACE = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_place"
 ]
H_R_CARTOGRAPHY_PLACE = [
    "index"
    , "id_uuid"
    , "id_cartography"
    , "id_place"
 ]
H_R_ADDRESS_PLACE = [
    "index"
    , "id_uuid"
    , "id_address"
    , "id_place"
 ]
H_R_ADMIN_PERSON = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_cartography"
    , "id_admin_person"
    , "id_directory"
 ]
H_R_INSTITUTION = [
    "index"
    , "id_uuid"
    , "id_iconography"
    , "id_cartography"
    , "id_directory"
    , "id_institution"
 ]
H_ADMIN_PERSON = [
    "index"
    , "id_uuid"
    , "first_name"
    , "last_name"
    , "id_persistent"
 ]
H_LICENSE = [
    "index"
    , "id_uuid"
    , "entry_name"
    , "description"
 ]

# a handy dict that holds all our necessary data:
# structure:
# {
#    <table_name>: [ <column_headers (list)>, <full_filename (str)>  ],
#    <table_name>: ...
# }
DATASETS = {
    # ** input datasets **
    "in_iconography": [ H_IN_ICONOGRAPHY, os.path.join(IN_ICONOGRAPHY, "iconography_clean.csv")  ],
    "in_cartography": [ H_IN_CARTOGRAPHY, os.path.join(IN_CARTOGRAPHY, "cartography_clean.csv")  ],
    "in_place":       [ H_IN_PLACE, os.path.join(IN_PLACE, "place_clean.csv")  ],
    "in_directory":   [ H_IN_DIRECTORY, os.path.join(IN_DIRECTORY, "directory.csv")  ],

    # ** temp datasets matching sql tables **
    # data sources
    "iconography": [ H_ICONOGRAPHY, os.path.join(TMP, "iconography.csv")  ],
    "cartography": [ H_CARTOGRAPHY, os.path.join(TMP, "cartography.csv")  ],
    "filename":        [ H_FILENAME, os.path.join(TMP, "filename.csv")  ],
    "directory":   [ H_DIRECTORY, os.path.join(TMP, "directory.csv")  ],

    # address/place datasets
    "address":     [ H_ADDRESS, os.path.join(TMP, "address.csv")  ],
    "place":       [ H_PLACE, os.path.join(TMP, "place.csv")  ],

    # relationship tables
    "r_iconography_actor":        [ H_R_ICONOGRAPHY_ACTOR, os.path.join(TMP, "r_iconography_actor.csv")  ],
    "r_iconography_named_entity": [ H_R_ICONOGRAPHY_NAMED_ENTITY, os.path.join(TMP, "r_iconography_named_entity.csv")  ],
    "r_iconography_theme":        [ H_R_ICONOGRAPHY_THEME, os.path.join(TMP, "r_iconography_theme.csv")  ],
    "r_iconography_place":        [ H_R_ICONOGRAPHY_PLACE, os.path.join(TMP, "r_iconography_place.csv")  ],
    "r_cartography_place":        [ H_R_CARTOGRAPHY_PLACE, os.path.join(TMP, "r_cartography_place.csv")  ],
    "r_address_place":            [ H_R_ADDRESS_PLACE, os.path.join(TMP, "r_address_place.csv")  ],
    "r_admin_person":             [ H_R_ADMIN_PERSON, os.path.join(TMP, "r_admin_person.csv")  ],
    "r_institution":              [ H_R_INSTITUTION, os.path.join(TMP, "r_institution.csv")  ],

    # tagging/descriptor tables
    "actor":          [ H_ACTOR, os.path.join(TMP, "actor.csv")  ],
    "named_entity":   [ H_NAMED_ENTITY, os.path.join(TMP, "named_entity.csv")  ],
    "theme":          [ H_THEME, os.path.join(TMP, "theme.csv")  ],
    "annotation":     [ H_ANNOTATION, os.path.join(TMP, "annotation.csv")  ],
    "title":          [ H_TITLE, os.path.join(TMP, "title.csv")  ],
    "place_group":    [ H_PLACE_GROUP, os.path.join(TMP, "place_group.csv")  ],

    # admin datasets
    "institution":  [ H_INSTITUTION, os.path.join(TMP, "institution.csv") ],
    "admin_person": [ H_ADMIN_PERSON, os.path.join(TMP, "admin_person.csv")  ],
    "licence":      [ H_LICENSE, os.path.join(TMP, "licence.csv")  ],

}

