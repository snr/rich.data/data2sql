from unidecode import unidecode
from typing import List
import rapidfuzz as rf
import pandas as pd
import uuid
import re


# **********************************************
# basic and generally useful string operations
# **********************************************


def build_uuid() -> str:
    """
    create a UUID tailored to our database:
    a 32 character hexadecimal string (without the `-`)
    and prefixed by `qr`
    these will be used to generate arks identifiers in Agorha.

    there are many ways to generate UUIDs. for simplicity, we
    use `uuid.uuid4()`, which just outputs a random sequence
    of characters.
    """
    return f"qr1{uuid.uuid4().hex}"


def _validate_uuid(_uuid:str, tablename:str) -> str:
    """
    check that an uuid follows this structure:
    `qr1` + a 32char hexadecimal UUID, with no `-`
    separators, in lowercase

    :param _uuid: the identifier to validate
    :param tablename: the name of the table which we're validating.
    :returns: the same identifier if it was validated.
    :throws: ValueError if the identifier doesn't follow
             the structure above
    """
    assert re.search("^qr1[a-z0-9]{32}$", _uuid), \
           ( "Validation error on `%s.uuid`. A primary "
           , "key must match `^qr1[a-z0-9]{32}$`."
           , "Input value: `%s`" % ( tablename, _uuid ))
    return _uuid


def isempty(v:str) -> bool:
    """
    check if a value is empty

    :param v: the value to check
    """
    return ( re.search("^(\s|\n)*$", v)           # actually empty string
             if not (pd.isnull(v) or pd.isna(v))  # empty according to pandas
             else True )                          # true if pd.isnull or pd.isna


def isanon(val:str) -> str:
    """
    check for different notations indicating
    an "unknown value" ("Anonyme"...)

    :param val: the string to process
    """
    return re.compile("""
        ^\s*(
            [Ii]nconnue?
            |[Aa]nonyme
            |\[s\.n\.\]
        )\s*$
    """, flags=re.VERBOSE).search(val)


def emptypipe(val:str) -> str:
    """
    remove empty piped values from a cell
    for example:
    * `Anne de Bretagne| ()` -> `| ()` will be removed
    * `Charivari||Martinet` -> one `|` will be removed to only keep one pipe
    * `|Anne de Bretagne` -> `|` will be removed, since there's nothing before it
    """
    val = re.sub("\|+", "|", val)  # multiple pipes one after another
    return re.compile("""
        (
            ^[\s_\-':;,.!?&/\\\"\[\]\(\)]*\|        # empty pipe at beginning
            |\|[\s_\-':;,.!?&/\\\"\[\]\(\)]*$       # empty pipe at end
            |\|[\s_\-':;,.!?&/\\\"\[\]\(\)]*(?=\|)  # empty pipe in the middle
        )
    """, flags=re.VERBOSE).sub("", val)


def fuzzydate(cell:str) -> List[str]:
    """
    extract a precise or fuzzy YYYY date from a string.
    a fuzzy date is a date with `.` instead of a number:
    `1999`, `18..`...
    :param cell: the cell to process
    :returns: the list of extracted dates from the cell
    """
    try:
        return re.findall("(?<!\d)(?<!\.)[\d\.]{4}(?!\d)(?!\.)", str(cell))
    except:
        print(f"""
        #####################################
        fuzzydate() type error
        excepected `str`, got `{type(cell)}`
        on value:
        `{cell}`
        #####################################
        """)
        return []


def splitcell(cell:str) -> List[str]:
    """
    split a multivalued cell into a list,
    using "|" and "\n" as serparators. in
    the process, pop out the empty list items

    :param cell: the cell to split
    :returns: the cell as a list.
    """
    return [
        r.strip()
        for r
        in re.split( "[\\n|\|]", emptypipe(cell.strip()) )
        if not isempty(r)
    ]


def simplify(val:str) -> str:
    """
    simplify a string: normalize accents and space,
    remove non-alphanumeric chars, strip, convert to lowercase
    """
    return re.sub( "\s+"
                 , " "
                 , unidecode( re.sub("[^\w\s]+", "", val) )
           ).lower().strip()


def abv2full(val:str) -> str:
    """
    replace abbreviations in `directory.street` by their full version
    """
    abv = {
        "[Rr]\."       : "Rue",
        "[Qq]\."       : "Quai",
        "[Pp]l\."      : "Place",
        "[Ss]t\."      : "Saint",
        "[Ss]te\."     : "Sainte",
        "[Aa]v\."      : "Avenue",
        "[Pp]al\."     : "Palais",
        "[Gg]al\."     : "Galerie",
        "[Gg]aler\."   : "Galerie",
        "[Ff]aub\."    : "Faubourg",
        "[Pp]ass?\."   : "Passage",
        "[Aa]ve?n?\."  : "Avenue",
        "[Bb]oule?v?\.": "Boulevard",
    }
    for k, v in abv.items():
        val = re.sub(f"([-\s^])({k})([-\s$])", f"\1{v}\2", val)
    return val


def roof(val:str) -> int:
    """
    calculate the roof value of an imprecise date,
    rounding up at the upper decade / century:
    * `18..` -> `1900`
    * `188.` -> `1890`

    tweaked from `date_normalize()` in `1_data_preparation`:
    * https://gitlab.inha.fr/snr/rich.data/data-preparation/-/blob/main/src/utils/strings.py
    \
    :param val: the string to process, a string of 4 characters
    :returns: the extracted date as an integer
    """
    num = re.search("\d+", val)[0]     # identified numbers in the date: `18`
    nan = re.search("[^\d]+", val)[0]  # unidentified numbers: `..`
    roof = int(num) + 1                # upper decade/century
    zeroes = re.sub(".", "0", nan)     # numbers to add at the end of `num` to have a valid YYYY date
    return int(str(roof) + zeroes)


def floor(val:str) -> int:
    """
    calculate the floor value of an imprecise date,
    rounding up at the lower decade / century
    * `18..` -> `1801`
    * `189.` -> `1891`

    tweaked from `date_normalize()` in `1_data_preparation`:
    * https://gitlab.inha.fr/snr/rich.data/data-preparation/-/blob/main/src/utils/strings.py

    :param val: the string to process, a string of 4 characters
    :returns: the extracted date as an integer
    """
    num = re.search("\d+", val)[0]     # identified numbers in the date: `18`
    nan = re.search("[^\d]+", val)[0]  # unidentified numbers: `..`
    zeroes = re.sub("\.", "0", nan)    # numbers to add at the end of `num` to have a valid YYYY date
    return int(num + zeroes) + 1


def date2range(date_array:List[str]) -> List[int]:
    """
    convert a list of dates into a range between two extreme dates.
    in the process,
    * reorder the dates, even if they are inprecise:
      ['18..', '1785'] -> ['1785', '18..']
    * convert imprecise dates by extracting the roof/floor date
      ['1785', '18..'] -> [1785, 1900]

    other examples:
    * ['19..', '1920'] -> [1901, 1920]
    * ['1901', '191.'] -> [1901, 1920]
    * ['1901', '1905', '193.'] -> [1901, 1940]

    tweaked from `date_normalize()` in `1_data_preparation`:
    * https://gitlab.inha.fr/snr/rich.data/data-preparation/-/blob/main/src/utils/strings.py

    :param date_array: a list of two or more dates. we assume that
                       they are ordered in chronological order
    :returns: the processed array
    """
    date_array = list(set(date_array))  # deduplicate


    # reorder, taking into account the unknown numbers
    if any(re.search("\.", d) for d in date_array):
        # smallest number of known numbers in the date: `['19..', '1...']` -> 1
        # -> we'll need to order the list based on the first character of `date_array`
        tokeep = min([ len(d)
                       for d
                       in [ re.sub("\.+", "", d) for d in date_array ] ])
        # dict mapping the original dates to their smallest
        # common denominator to reorder
        datedict = { k: v
                     for k, v
                     in zip(date_array, [ d[0:tokeep] for d in date_array ]) }
        # reorder by value (the value being the smallest
        # known denominator between the dates)
        if len( set(datedict.values()) ) > 1:
            datedict = { k: v
                         for k, v
                         in sorted( datedict.items(), key = lambda item: item[1] ) }
        date_array = list(datedict.keys())

    # extract the floor and the roof if necessary
    if len(date_array) > 0:
        # lower date
        try:
            date_array[0] = int(date_array[0])
        except ValueError:
            date_array[0] = floor(date_array[0])
        # upper date
        try:
            date_array[-1] = int(date_array[-1])
        except ValueError:
            date_array[-1] = roof(date_array[-1])

        date_array = [ date_array[0], date_array[-1] ]

    else:
        date_array = []

    return date_array


def clustering(
        s:str
        , sgroup:List[str]
    ) -> List[str]:
    """
    clusterize strings by similarity:
    create a group of strings in `sgroup` based on
    their similarity to `s` and return it
    """
    return [
        r[0]
        for r
        in rf.process.extract(s
                              , sgroup
                              , processor=None
                              , score_cutoff=90.1)  # below 90.0, there's a lot of noise
    ]


