from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from scp import SCPClient
import pandas as pd
import paramiko
import zipfile
import shutil
import json
import sys
import os

from .utils.constants import TMP, IN_CARTOGRAPHY, IN_ICONOGRAPHY, UTILS
from .utils.io import read_df


# *****************************************************
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# UNUSED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# push image files to the INHA servers using SCP
# *****************************************************


def check_ssh_output(
    _in:str
    , stdout:paramiko.ChannelFile
    , stderr:paramiko.ChannelFile
):
    """
    check if there was an error on an ssh command.
    if so, print an error dump and raise an exception
    else, print the command's stdout

    `_in` is the string written to `exec_command`,
    which can't be accessed from the ChanelFile object
    """
    sts = stdout.channel.recv_exit_status()  # exit message
    out = stdout.read().decode("utf-8")      # text from stdout
    err = stderr.read().decode("utf-8")      # text from stderr

    if sts != 0:
        raise paramiko.SSHException("\nerror on SSH query:\n"
                                    + f"> sdtin  : `{_in}`\n"
                                    + f"> sdtout : `{out}`\n"
                                    + f"> sdterr : `{err}`\n")
    else:
        print(f"\n> ssh query: `{_in}`\n> output:\n{out}")
    return


def isok(cmd:str) -> bool|None:
    """
    small recursive function to validate user input
    and make sure that the user is ok with running
    the command `cmd`

    :param cmd: the command to run
    :returns: `ok`: True if the user wants to run `cmd`,
                    else the function is run again or
                    the script exits
    """
    ok = input(f"\n> run ssh command: `{cmd}` ? [y/n]").strip().lower()
    if ok not in ["y", "n"]:
        print("> invalid user input!")
        return isok(cmd)
    else:
        if ok == "y":
            return True
        else:
            print(f"> user has refused to run ssh command `{cmd}`. \n> exiting...")
            exit()
    return


def run_if_ok(client:paramiko.SSHClient, cmd:str) -> None:
    """
    run an ssh command on the server's shell.
    each command is validated to avoid messing
    things up if the command is wrong or whatever.

    :param client: the paramiko ssh client
    :param cmd:    the command to run
    """
    if isok(cmd):
        stdin, stdout, stderr = client.exec_command(cmd)
        check_ssh_output(cmd, stdout, stderr)
    return


def scpprogress(fn, size, sent):
    """
    scp progress printer, copied from the scp docs:
    https://pypi.org/project/scp/
    """
    sys.stdout.write("%s's progress: %.2f%%   \r" % (fn, float(sent)/float(size)*100) )
    return


def pipeline_files2server():
    """
    process:
    * copy all raster files from `iconography` and
      `cartography` to `TMP`
    * zip all the files
    * push them to the distant server using `scp` and `ssh`
    * unzip the files there

    about paramiko and scp:
    * paramiko docs: https://www.paramiko.org/
    * tutorials: https://github.com/paramiko/paramiko/blob/main/demos/

    """
    print("pushing the image files to the server"
          , "\n*************************************")


    # ************************* VARIABLES ************************* #
    indir_cartography = os.path.join(IN_CARTOGRAPHY, "raster")
    indir_iconography = os.path.join(IN_ICONOGRAPHY, "images")
    tmpdir_images = os.path.join(TMP, "imagefiles")
    tmpdir_cartography = os.path.join(tmpdir_images, "cartography")
    tmpdir_iconography = os.path.join(tmpdir_images, "iconography")
    zippath = os.path.join(TMP, "imagefiles.zip")

    df = read_df("file", useindexcol=0, customheader=False)
    files_iconography = df.loc[ df["id_iconography"].notna(), "url" ].unique()
    files_cartography = df.loc[ df["id_cartography"].notna(), "url" ].unique()

    with open(os.path.join(UTILS, "confidentials", "ssh_credentials.json"), mode="r") as fh:
        ssh_credentials = json.load(fh)

    # ************************* ZIP FILES ************************* #
    # copy to `tmp/imagefiles`
    for f in files_iconography:
        shutil.copy2( os.path.join(indir_iconography, f)
                      , os.path.join(tmpdir_iconography, f) )
    for f in files_cartography:
        shutil.copy2( os.path.join(indir_cartography, f)
                      , os.path.join(tmpdir_cartography, f))

    # zip all files. based on: https://stackoverflow.com/a/1855118/17915803
    n = 0
    ttl = len(files_iconography) + len(files_cartography)
    with zipfile.ZipFile(zippath
                         , "w"
                         , zipfile.ZIP_DEFLATED) as zf:
        for root, dirs, files in os.walk(tmpdir_images):
            for f in files:
                n += 1
                print(f"* zipping image files: {n}/{ttl}", end="\r")
                zf.write(os.path.join(root, f),                              # from
                         os.path.relpath(os.path.join(root, f),              # to
                                         os.path.join(tmpdir_images, ".."))
                )
    print("")

    # ************************* PUSH TO SERVER ************************* #
    print("\n* running SSH commands to send the image files and extract them on the server."
          + "user validation is REQUIRED for each command.")
    ssh = paramiko.SSHClient()                               # create a client
    ssh.load_system_host_keys()                              #
    # client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    try:
        ssh.connect(hostname=ssh_credentials["server"]              # ssh client
                    , username=ssh_credentials["username"]
                    , password=ssh_credentials["password"]
                    , auth_timeout=15)
        scp = SCPClient(ssh.get_transport(), progress=scpprogress)  # scp client
    except TimeoutError:
        raise TimeoutError("\n!!!! timeout lors de la tentative de connexion SSH."
                           + "vérifier que vous êtes connectés au VPN INHA ou sur "
                           + "le réseau filaire de l'INHA !!!!\n")

    # run the commands
    cmd = (f"""
        if [ -d { ssh_credentials['imagepath'] } ];
        then
            echo "directory { ssh_credentials['imagepath'] } exists. emptying it...";
            rm -r { ssh_credentials['imagepath'] };
        else
            echo "directory { ssh_credentials['imagepath'] } doesn't exist. doing nothing..."
        fi;
    """)
    run_if_ok(ssh, cmd)

    if isok(f"<send zipped image files to server through scp>"):
        scp.put(zippath, os.path.join(ssh_credentials["basepath"], "imagefiles.zip"))
    else:
        print("> user has refused to run the command. exiting...")
    print("")

    cmd = f"unzip { ssh_credentials['basepath'] }/imagefiles.zip -d { ssh_credentials['basepath'] }"
    run_if_ok(ssh, cmd)

    cmd = f"rm { ssh_credentials['basepath'] }/imagefiles.zip"
    run_if_ok(ssh, cmd)

    print(f"image files uploaded to `{ ssh_credentials['imagepath'] }`")

    scp.close()
    ssh.close()
    return




