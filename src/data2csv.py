import os

from .processes_data2csv.relationships import RelationshipProcesses
from .processes_data2csv.cartography import CartographyProcesses
from .processes_data2csv.iconography import IconographyProcesses
from .processes_data2csv.directory import DirectoryProcesses
from .processes_data2csv.place import PlaceProcesses
from .processes_data2csv.other import OtherProcesses

from .utils.constants import TMP
from .utils.io import deltmp

# **************************************************
# here, we transform our datasets into csv tables.
# each csv corresponds to a single sql table.
#
# this file contains the general pipeline. each data
# source is contained in an object `*Processes`
# (ex: `IconographyProcesses`) which contains the
# dataframe and all functions to process it into
# tables. all of those tables inherit from the
# `SharedProcesses` class, which defines pandas
# wrappers and commonly used functions.
#
# all CSVs are stored in the `src/temp` folder,
# which is created in this step and can be deleted in
# `csv2sql.py`
#
# insertion of data from these csv to sql will
# be performed in the next step: `csv2sql.py`
# **************************************************


def pipeline_data2csv() -> None:
    """
    main pipeline for this step:
    * parse data sources (csv's and prepared data tables
      stored in `constants`, process them and write them
      to csv files. 1 file per table, 1 row = 1 sql row)
    """

    # create the output datasets
    print("\ncreating the output files\n"
          "*************************")
    OtherProcesses().pipeline()

    # process each data source invividually
    # ***** THE EXECUTION ORDER IS IMPORTANT *****

    # this step is not done because
    # 1) the directory is too messy to be included in the db and won't appear in the website
    # 2) the `address` table's structure has changed (addresses are not parsed into several columns anymore) which would need to rewrite this step
    print("\nprocessing the directory dataset\n"
          "********************************")
    DirectoryProcesses(samplemode="empty").pipeline()

    print("\nprocessing the place dataset\n"
          "*******************************")
    PlaceProcesses().pipeline()

    print("\nprocessing the iconography dataset\n"
          "**********************************")
    IconographyProcesses().pipeline()

    print("\nprocessing the cartography dataset\n"
          "**********************************")
    CartographyProcesses().pipeline()

    # create relationship tables
    print("\ncreating relationship tables\n"
          "****************************")
    RelationshipProcesses().pipeline()

    return


