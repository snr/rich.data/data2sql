#!/usr/bin/env bash

# run a dump of the `richelieu` database
pg_dump -C -h localhost -U richelieu_dbapi richelieu_db --no-owner --clean > richelieu_db_dump_v2.sql
