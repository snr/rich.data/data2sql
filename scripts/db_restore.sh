#!/usr/bin/env bash

# restore the database from a dump. erases all previous contents of the db 
# may need to use `dumpdb <db_name> && createdb <db_name>` 
psql -h localhost -U richelieu_dbapi -d richelieu_db < ../out/richelieu_dump.sql
