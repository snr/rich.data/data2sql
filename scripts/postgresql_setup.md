# Installation et paramétrage de PostgreSQL et pgAdmin

Tutoriel pour `postgresql-14` sur Ubuntu.

Dans le code, quand je mets `<>`, on peut replacer l'ensemble par
ses propres infos: remplacer `<user_name>` par 'toto', par exemple.

---

## Vocabulaire de base

- `postgresql`, c'est un système de gestion de base de données. 
  - il utilise une architecture [client-serveur](https://fr.wikipedia.org/wiki/Client-serveur),
    c'est-à-dire que la BDD est stockée sur un serveur et peut être accédée
    depuis plusieurs applications (les clients) via internet. 
  - même dans une installation locale, la base est sur un serveur et elle
    est accédée par d'autres programmes.
  - voir [la doc](https://www.postgresql.org/docs/current/tutorial-arch.html)
- `psql`, le client en ligne de commandes pour utiliser `postgresql`
  (se connecter à une BDD, faire tout le SQL qu'on veut...). Un client,
  c'est une appli qui permet de communiquer avec un serveur. `psql` est
  installé par défaut avec `postgreSQL`.
- `pgAdmin`, c'est un logiciel client qui sert d'interface graphique à
  `postgresql`. Le logiciel doit être installé séparément de `postgresql`.
- `postgres`, c'est 2 choses différentes:
  - le nom de la partie serveur de PostgreSQL (par opposition à la partie client). 
  - le compte administrateur par défaut sur `postgresql`. Il est présent 
    dès l'installation de `postgresql`.
- les *utilisateur.ice.s* de postgresql doivent être représentés par une 
  base de données postgresql ! 
  - Si on a un *user* `fan_de_tuning_97`, il faut qu'il y ait une *base de données* 
    `fan_de_tuning_97`. 
  - Attention, un.e utilisateur.ice postgresql n'est pas la même chose qu'un
    compte utilisateur dans une base de données. Dans le premier cas, c'est
    un.e utilisateur.ice qui a accès à plusieurs BDD et qui est connu.e par
    le logiciel postgresql; alors que le compte utilisateur, n'existe qu'au
    sein d'une base de données précise.

---

## Installation

### `postgresql`

**Voir [ici](https://www.postgresql.org/download/linux/ubuntu/) 
pour la documentation officielle**

```bash
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt-get update

# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql-14

psql --version  # check that all is good
```

### `pgAdmin`

**Voir [ici](https://www.pgadmin.org/download/pgadmin-4-apt/) 
pour la documentation officielle**
```bash
# Install the public key for the repository (if not done previously):
curl -fsS https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo gpg --dearmor -o /usr/share/keyrings/packages-pgadmin-org.gpg

# Create the repository configuration file:
sudo sh -c 'echo "deb [signed-by=/usr/share/keyrings/packages-pgadmin-org.gpg] https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'

# choose which version to install below
# Install for both desktop and web modes:
sudo apt install pgadmin4

# Install for desktop mode only:
sudo apt install pgadmin4-desktop

# Install for web mode only:
sudo apt install pgadmin4-web

# Configure the webserver, if you installed pgadmin4-web:
sudo /usr/pgadmin4/bin/setup-web.sh
```

---

## Paramétrage de `postgresql`

Après l'installation, il y a quelques opérations à faire.

### Modifier `pg_hba.conf`. 

Voir la doc officielle de [postgresql](https://www.postgresql.org/docs/current/client-authentication.html)
et la doc du fichier [`pg_hba`](https://www.postgresql.org/docs/current/auth-pg-hba-conf.html).

C'est le fichier de conf de postgresql. 
Il se trouve dans `/etc/postgresql/14/main/pg_hba.conf`, il faut
l'ouvrir dans un éditeur de texte pour avoir la configuration ci-dessous.
C'est dans les premières lignes non-commentées du fichier.
  
```bash
# Database administrative login by Unix domain socket
local   all             postgres                                  trust

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     scram-sha-256
```

Ensuite, il faut relancer `postgresql`:
```bash
systemctl restart postgresql@14-main.service
```

### Créer les comptes utilisateur.ice.s et les bases de données nécessaires

- créer le rôle d'utilisateur.ice dans `psql`:
  ```bash
  psql -U postgres  # se connecter en tant que postgres

  # on est maintenant dans psql
  >>> CREATE USER <user_name> WIIH PASSWORD '<my password>';
  ```
- créer la base de données associée, dans le terminal:
  ```bash
  createdb -U postgres <user_name>
  ```
- vérifier que tout va bien, en ligne de commande:
  ```bash
  psql -U <user_name>  # si on arrive à se connecter, tt va bien
  ```
- ou, dans `psql`, `\du` pour liser les utilisateur.ice.s.

### Créer une base de données

- dans `psql`:
  ```bash
  createdb <database_name>
  ```
- se connecter pour vérifier que tout va bien:
  ```bash
  psql -U postgres -d <database_name>
  ```
- ou, dans psql, utiliser `\l` pour vérifier que la base a été créé.


### Modifier les privilèges d'utilisateur-ice

Voir la [doc officielle](https://www.postgresql.org/docs/current/ddl-priv.html)

- se connecter à psql:
  ```bash
  psql -U postgres
  ```
- lancer les commandes nécessaires dans psql:
  ```sql
  # modifier lae propriétaire d'une base
  ALTER TABLE <table_name> OWNER TO <user_name>;
  
  # modifier les droits sur une base pour un.e utilisateur-ice
  # (choisir le droit qu'on veut dans la liste)
  GRANT SELECT|INSERT|UPDATE|DELETE|... ON <table_name> TO <user_name>;

  # retirer les privilèges
  GRANT SELECT|INSERT|UPDATE|DELETE|... ON <table_name> TO <user_name>;
  ```

---

## Sauvegarder (`dump`) et restaurer une base de données

Faire un `dump`, c'est enregistrer un fichier SQL qui contient l'intégralité de 
la base de données (création des tables, insertion des données...). La restaurer,
c'est lire ce dump pour créer une base de données. Ça peut permettre de copier
une base d'un serveur local `A` à un autre serveur `B`, en dumpant la base dans `A`
et en la restaurant dans `B`.

Toutes les commandes ci-dessous sont à faire dans le terminal, en dehors de `pg_dump`.

- **Faire le dump avec `pg_dump`**
  - `-C`: *a command to create the database itself and reconnect to the created database*
    (je ne sais pas trop ce que ça fait mais bon)
  - `-h localhost`: commande optionnelle. `-h` spécifie le `host` (le serveur) auquel se
    connecter, `localhost` montre qu'on se connecte à un serveur local
  - `-U <username>`: l'utilisateur.ice utilisé pour le dump
  - `<database name>`: le nom de la BDD dont on fait un dump
  - `--no-owner`: ne pas inclure d'informations sur les privilèges d'accès à la BDD.
    **très utile quand on restaure la base sur un autre serveur !**
  ```bash
  # syntaxe
  pg_dump -C -h localhost -U <username> <database_name> --no-owner > <output_SQL_file>
  
  # par exemple:
  pg_dump -C -h localhost -U richelieu_dbapi richelieu_db --no-owner > richelieu_db_dump_v2.sql
  ```

- **Restaurer avec `psql`**: il suffit de lire en entrée le SQL généré avec `pg_dump`
  - ça peut être nécessaire de supprimer la base de données avant de la recréer: 
  `dropdb <db_name> && createdb <db_name>`
  ```bash
  # syntaxe
  psql -U <username> -d <database_name> < <sql_file_dump>

  # par exemple
  psql -U richelieu_dbapi -d richelieu_db < richelieu_dump.sql
  ```

**Attetion**: le dump et la restauration peuvent créer des erreurs, surtout au niveau des 
permissions SQL. Quelques solutions:
- utiliser l'option `--no-owner` avec `pg_dump`
- recréer la base en tant qu'utilisateur `postgres`, quitte à modifier les privilèges ensuite.




