--
-- check the place table
--

SELECT place.vector_source
       , place.centroid
       , CONCAT(address.number, ' ', address.street) AS address
       , cartography.title AS cartography
FROM r_address_place

JOIN place 
  ON r_address_place.id_place = place.id
JOIN address
  ON r_address_place.id_address = address.id
JOIN r_cartography_place
  ON r_cartography_place.id_place = place.id
JOIN cartography
  ON r_cartography_place.id_cartography = cartography.id

WHERE place.vector_source = address.source
  AND place.vector_source = cartography.map_source;