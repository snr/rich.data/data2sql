#!/usr/bin/env bash

cat << EOF

#################################################
#      number of lines of files in src/tmp      #
#################################################

EOF

root=$(dirname "$(realpath $0)")/..;
maxlines=$(( $( ls $root/src/tmp/* | less | wc -L ) + 1 ));
for f in $root/src/tmp/*; do 
  filelen=$( cat "$f" | wc -l );
  len=$(( $maxlines - ${#f} ));
  spaces=$( printf '%.0s.' $(seq 1 $len) );
  echo "${f}${spaces}: ${filelen}";
done;
