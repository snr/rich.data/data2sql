# SCRIPTS

Petits scripts et utilitaires fort sympatiques

- `db_dump.sh`: faire un dump d'une base de données
- `db_restore.sh`: restaurer une base de données à partir d'un dump 
- `filenamecount.sh`: compter le nombre de lignes dans `src/tmp` 
- `install_gdal.sh`: installer la librairie C GDAL
- `install_postgresql_14.sh`: installer PostgreSQL 14
- `postgresql_setup.md`: quelques conseils de prise en main de PostGreSQL 
- `sql_queries/`: quelques requêtes sql

